package com.quantega.quantcare.services.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.quantega.quantcare.services.dto.CMSPage;
import com.quantega.quantcare.services.service.CMSService;
import com.quantega.quantcare.services.service.dto.ResponseObject;

/*
 * My Inc Confidential
 * Class: AuthController.java
 * (C) Copyright My Inc. 2018
 * 
 * @author kk
 */
@RestController
@RequestMapping("/cms")
public class CMSController {

  private final Logger LOGGER = LoggerFactory.getLogger(getClass());

  @Autowired
  private CMSService cmsService;

  public CMSController() {
  }
  
  /**
   * This is a test service, ping to check if the server is up and running.<br/>
   * 
   * @param request   The HttpServletRequest object received.
   * @param response  The HttpServletResponse object received.
   * @return  The ping response in JSON Format
   */
  @CrossOrigin
  @ResponseBody
  @RequestMapping(value = "/ping", method = RequestMethod.GET)
  public ResponseObject ping() {
      LOGGER.info("Executing ping()");
    ResponseObject responseObj = new ResponseObject(HttpStatus.OK.value());
    responseObj.setMessage("SUCCESS!!");
    return responseObj;
  }

  /**
   * The create method creates a new page.<br/>
   * The page content is received in REST call and saved back to the CMS through the CMS provided API calls<br/>
   * <br/>
   * 
   * @param page   The page to create.
   * @return  The created page.
   */
  @CrossOrigin
  @ResponseBody
  @RequestMapping(value = "/create", method = RequestMethod.POST)
  public ResponseObject create( 
      @RequestHeader(value="username") String username, @RequestHeader(value="password") String password, 
      @RequestBody CMSPage page) {
    LOGGER.info("Executing create()");
    return cmsService.create(page); 
  }

  /**
   * The edit method edits an existing page.<br/>
   * The page content is received in REST call and saved back to the CMS through the CMS provided API calls<br/>
   * <br/>
   * 
   * @param page   The page to edit.
   * @return  The edited page.
   */
  @CrossOrigin
  @ResponseBody
  @RequestMapping(value = "/edit", method = RequestMethod.POST)
  public ResponseObject edit( 
      @RequestHeader(value="username") String username, 
      @RequestHeader(value="password") String password, 
      @RequestBody CMSPage page) {
    LOGGER.info("Executing edit()");
    return cmsService.edit(page); 
  }

  /**
   * The publish method publishes the page just created.<br/>
   * The publish request is received in REST call and publishes the CMS page through the CMS provided API calls<br/>
   * <br/>
   * 
   * @param page   The page to publish.
   * @return  The published page.
   */
  @CrossOrigin
  @ResponseBody
  @RequestMapping(value = "/publish", method = RequestMethod.POST)
  public ResponseObject publish( 
      @RequestHeader(value="username") String username, 
      @RequestHeader(value="password") String password, 
      @RequestBody CMSPage page) {
    LOGGER.info("Executing publish()");
    return cmsService.publish(page); 
  }

  /**
   * The unpublish method publishes the page created.<br/>
   * The unpublish request is received in REST call and unpublishes the CMS page through the CMS provided API calls<br/>
   * <br/>
   * 
   * @param page   The page to unpublish.
   * @return  The unpublished page.
   */
  @CrossOrigin
  @ResponseBody
  @RequestMapping(value = "/unpublish", method = RequestMethod.POST)
  public ResponseObject unpublish( 
      @RequestHeader(value="username") String username, 
      @RequestHeader(value="password") String password, 
      @RequestBody CMSPage page) {
    LOGGER.info("Executing unpublish()");
    return cmsService.unpublish(page); 
  }

  /**
   * The delete method delete an existing page.<br/>
   * The page content is received in REST call and deleted from the CMS through the CMS provided API calls<br/>
   * <br/>
   * 
   * @param page   The page to delete.
   * @return  The deleted page.
   */
  @CrossOrigin
  @ResponseBody
  @RequestMapping(value = "/delete", method = RequestMethod.POST)
  public ResponseObject delete( 
      @RequestHeader(value="username") String username, 
      @RequestHeader(value="password") String password, 
      @RequestBody CMSPage page) {
    LOGGER.info("Executing page()");
    return cmsService.delete(page); 
  }

}
