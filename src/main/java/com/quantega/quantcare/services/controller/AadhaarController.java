package com.quantega.quantcare.services.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.quantega.quantcare.services.dto.Aadhaar;
import com.quantega.quantcare.services.service.AadhaarService;
import com.quantega.quantcare.services.service.dto.ResponseObject;

/*
 * My Inc Confidential
 * Class: AadhaarController.java
 * (C) Copyright My Inc. 2018
 * 
 * @author kk
 */
@RestController
@RequestMapping("/aadhar")
public class AadhaarController {

  private final Logger LOGGER = LoggerFactory.getLogger(getClass());

  @Autowired
  private AadhaarService aadhaarService;

  public AadhaarController() {
  }
  
  /**
   * The matchAadhaar method publishes the page created.<br/>
   * The matchaadhaar request is received in REST call and matchaadhaares the Aadhaar through the UIDAI API calls<br/>
   * <br/>
   * 
   * @param page   The page to matchaadhaar.
   * @return  The matchaadhaared page.
   */
  @CrossOrigin
  @ResponseBody
  @RequestMapping(value = "/getaadhaar", method = RequestMethod.POST)
  public ResponseObject getAadhaar( 
      @RequestBody Aadhaar aadhaar) {
    LOGGER.info("Executing matchaadhaar()");
    return aadhaarService.getAadhaar(aadhaar); 
  }
  
  /**
   * The matchAadhaar method publishes the page created.<br/>
   * The matchaadhaar request is received in REST call and matchaadhaares the Aadhaar through the UIDAI API calls<br/>
   * <br/>
   * 
   * @param page   The page to matchaadhaar.
   * @return  The matchaadhaared page.
   */
  @CrossOrigin
  @ResponseBody
  @RequestMapping(value = "/matchaadhaar", method = RequestMethod.POST)
  public ResponseObject matchAadhaar( 
      @RequestBody Aadhaar aadhaar) {
    LOGGER.info("Executing matchaadhaar()");
    return aadhaarService.matchAadhaar(aadhaar); 
  }
}
