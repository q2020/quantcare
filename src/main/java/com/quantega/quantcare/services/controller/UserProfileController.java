package com.quantega.quantcare.services.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.quantega.quantcare.services.dto.UserProfile;
import com.quantega.quantcare.services.service.UserProfileService;
import com.quantega.quantcare.services.service.dto.ResponseObject;

/*
 * My Inc Confidential
 * Class: UserProfileController.java
 * (C) Copyright My Inc. 2018
 * 
 * @author kk
 */
@RestController
public class UserProfileController {

  private final Logger LOGGER = LoggerFactory.getLogger(getClass());

  @Autowired
  private UserProfileService userProfileService;

  public UserProfileController() {
  }
  
  /**
   * The create method creates a new user.<br/>
   * The user content is received in REST call and saved back to the CMS through the CMS provided API calls<br/>
   * <br/>
   * 
   * @param user   The user to create.
   * @return  The created user.
   */
  /*
  @CrossOrigin
  @ResponseBody
  @RequestMapping(value = "/create-user-profile", method = RequestMethod.POST)
  public ResponseObject create( 
      @RequestBody UserProfile user) {
    LOGGER.info("Executing create-user-profile()");
    return userProfileService.create(user); 
  }
  */

  /**
   * The edit method edits an existing user.<br/>
   * The user content is received in REST call and saved back to the CMS through the CMS provided API calls<br/>
   * <br/>
   * 
   * @param user   The user to edit.
   * @return  The edited user.
   */
  @CrossOrigin
  @ResponseBody
  @RequestMapping(value = "/edit-user-profile", method = RequestMethod.POST)
  public ResponseObject edit( 
      @RequestBody UserProfile user) {
    LOGGER.info("Executing edit-user-profile()");
    return userProfileService.edit(user); 
  }

  /**
   * The publish method publishes the user just created.<br/>
   * The publish request is received in REST call and publishes the CMS user through the CMS provided API calls<br/>
   * <br/>
   * 
   * @param user   The user to publish.
   * @return  The published user.
   */
  @CrossOrigin
  @ResponseBody
  @RequestMapping(value = "/get-user-profile", method = RequestMethod.POST)
  public ResponseObject get( 
      @RequestBody UserProfile user) {
    LOGGER.info("Executing get-user-profile()");
    return userProfileService.get(user); 
  }

  /**
   * The delete method delete an existing user.<br/>
   * The user content is received in REST call and deleted from the CMS through the CMS provided API calls<br/>
   * <br/>
   * 
   * @param user   The user to delete.
   * @return  The deleted user.
   */
  @CrossOrigin
  @ResponseBody
  @RequestMapping(value = "/delete-user-profile", method = RequestMethod.POST)
  public ResponseObject delete( 
      @RequestBody UserProfile user) {
    LOGGER.info("Executing delete-user-profile()");
    return userProfileService.delete(user); 
  }

}
