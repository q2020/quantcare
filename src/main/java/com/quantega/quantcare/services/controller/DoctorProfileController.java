package com.quantega.quantcare.services.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.quantega.quantcare.services.dto.DoctorProfile;
import com.quantega.quantcare.services.security.config.TokenAuthenticationFilter;
import com.quantega.quantcare.services.service.DoctorProfileService;
import com.quantega.quantcare.services.service.dto.ResponseObject;

/*
 * My Inc Confidential
 * Class: DoctorProfileController.java
 * (C) Copyright My Inc. 2018
 * 
 * @author kk
 */
@RestController
public class DoctorProfileController {

	private final Logger LOGGER = LoggerFactory.getLogger(getClass());

	@Autowired
	private DoctorProfileService doctorProfileService;

	public DoctorProfileController() {
	}

	/**
	 * The create method creates a new doctor.<br/>
	 * The doctor content is received in REST call and saved back to the CMS through
	 * the CMS provided API calls<br/>
	 * <br/>
	 * 
	 * @param doctor
	 *            The doctor to create.
	 * @return The created doctor.
	 */
	/*
	 * @CrossOrigin
	 * 
	 * @ResponseBody
	 * 
	 * @RequestMapping(value = "/create-doctor-profile", method =
	 * RequestMethod.POST) public ResponseObject create(
	 * 
	 * @RequestBody DoctorProfile doctor) {
	 * LOGGER.info("Executing create-doctor-profile()"); return
	 * doctorProfileService.create(doctor); }
	 */

	/**
	 * The edit method edits an existing doctor.<br/>
	 * The doctor content is received in REST call and saved back to the CMS through
	 * the CMS provided API calls<br/>
	 * <br/>
	 * 
	 * @param doctor
	 *            The doctor to edit.
	 * @return The edited doctor.
	 */
	@CrossOrigin
	@ResponseBody
	@RequestMapping(value = "/edit-doctor-profile", method = RequestMethod.POST)
	public ResponseObject edit(@RequestHeader(TokenAuthenticationFilter.AUTHORIZATION) String token, @RequestBody DoctorProfile doctor) {
		LOGGER.info("Executing edit-doctor-profile()");
		token = token.replace(TokenAuthenticationFilter.BEARER, "").trim();
		return doctorProfileService.edit(token, doctor);
	}

	/**
	 * The publish method publishes the doctor just created.<br/>
	 * The publish request is received in REST call and publishes the CMS doctor
	 * through the CMS provided API calls<br/>
	 * <br/>
	 * 
	 * @param doctor
	 *            The doctor to publish.
	 * @return The published doctor.
	 */
	@CrossOrigin
	@ResponseBody
	@RequestMapping(value = "/get-doctor-profile", method = RequestMethod.POST)
	public ResponseObject get(@RequestBody DoctorProfile doctor) {
		LOGGER.info("Executing get-doctor-profile()");
		return doctorProfileService.get(doctor);
	}

	/**
	 * The delete method delete an existing doctor.<br/>
	 * The doctor content is received in REST call and deleted from the CMS through
	 * the CMS provided API calls<br/>
	 * <br/>
	 * 
	 * @param doctor
	 *            The doctor to delete.
	 * @return The deleted doctor.
	 */
	@CrossOrigin
	@ResponseBody
	@RequestMapping(value = "/delete-doctor-profile", method = RequestMethod.POST)
	public ResponseObject delete(@RequestBody DoctorProfile doctor) {
		LOGGER.info("Executing delete-doctor-profile()");
		return doctorProfileService.delete(doctor);
	}

}
