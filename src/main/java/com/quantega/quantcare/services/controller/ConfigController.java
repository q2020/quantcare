package com.quantega.quantcare.services.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.quantega.quantcare.services.service.ConfigService;
import com.quantega.quantcare.services.service.dto.ResponseObject;

/*
 * My Inc Confidential
 * Class: ConfigController.java
 * (C) Copyright My Inc. 2018
 * 
 * @author kk
 */
@RestController
public class ConfigController {

	private final Logger LOGGER = LoggerFactory.getLogger(getClass());

	@Autowired
	private ConfigService configService;

	public ConfigController() {
	}

	/**
	 * The geo-by-ip method receives an IP in request.<br/>
	 * The doctor content is received in REST call and saved back to the CMS through
	 * the CMS provided API calls<br/>
	 * <br/>
	 * 
	 * @param doctor The doctor to edit.
	 * @return The edited doctor.
	 */
	@CrossOrigin
	@ResponseBody
	@RequestMapping(value = "/geo-by-ip/{ip}", method = RequestMethod.GET)
  public ResponseObject getGeoLocationByIP(@PathVariable("ip") String ip) {
		LOGGER.info("Executing getGeoLocationByIP()");
		return configService.getGeoLocationByIP(ip);
	}
}
