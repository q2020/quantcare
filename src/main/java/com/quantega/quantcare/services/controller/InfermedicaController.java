package com.quantega.quantcare.services.controller;

import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.quantega.quantcare.services.dto.InfermedicaGenericRequest;
import com.quantega.quantcare.services.dto.InfermedicaParseRequest;
import com.quantega.quantcare.services.service.InfermedicaService;
import com.quantega.quantcare.services.service.dto.ResponseObject;

/*
 * My Inc Confidential
 * Class: InfermedicaController.java
 * (C) Copyright My Inc. 2018
 * 
 * @author kk
 */
@RestController
public class InfermedicaController {

  /** LOGGER to use logging of Infermedica incidences */
  private final Logger LOGGER = LoggerFactory.getLogger(getClass());

  @Autowired
  private InfermedicaService infermedicaService;

  /**
   * Infermedica controller constructor.
   */
  public InfermedicaController() {
  }
  
  /**
   * The getConditions method returns a list of all available conditions to the caller.<br/>
   * 
   * @param ID   The ID of the condition to retrieve.
   * @return  The list of conditions.
   */
  @CrossOrigin
  @ResponseBody
  @RequestMapping(value = { "/conditions", "/conditions/{id}" }, method = RequestMethod.GET)
  public ResponseObject getConditions(@PathVariable("id") Optional<String> ID) {
    LOGGER.info("Executing getConditions()");
    return infermedicaService.getConditions(ID.orElse(null)); 
  }
  
  /**
   * Suggests possible diagnoses and relevant observations based on provided patient information.<br/>
   * 
   * @param genericRequest  The generic request body object.
   * @return response The ResponseObject with the diagnoses and relevant observations based on provided patient information.
   */
  @CrossOrigin
  @ResponseBody
  @RequestMapping(value = "diagnosis", method = RequestMethod.POST)
  public ResponseObject diagnosis(@RequestBody InfermedicaGenericRequest genericRequest) {
    LOGGER.info("Executing diagnosis()");
    return infermedicaService.diagnosis(genericRequest); 
  }
  
  /**
   * Explains which evidence impact probability of selected condition.<br/>
   * 
   * @param genericRequest  The generic request body object.
   * @return response  The ResponseObject with the evidence impact probability of selected condition.
   */
  @CrossOrigin
  @ResponseBody
  @RequestMapping(value = "explain", method = RequestMethod.POST)
  public ResponseObject explain(@RequestBody InfermedicaGenericRequest genericRequest) {
    LOGGER.info("Executing explain()");
    return infermedicaService.explain(genericRequest); 
  }
  
  /**
   * Returns information about data used by diagnostic engine.<br/>
   * 
   * @return  The list of conditions.
   */
  @CrossOrigin
  @ResponseBody
  @RequestMapping(value = "info", method = RequestMethod.GET)
  public ResponseObject info() {
    LOGGER.info("Executing info()");
    return infermedicaService.info(); 
  }
  
  /**
   * Returns a single observation matching given phrase.<br/>
   * 
   * @return A single observation matching given phrase.
   */
  @CrossOrigin
  @ResponseBody
  @RequestMapping(value = "lookup", method = RequestMethod.GET)
  public ResponseObject lookup() {
    LOGGER.info("Executing lookup()");
    return infermedicaService.lookup(); 
  }
  
  /**
   * Returns list of mentions of observation found in given text.<br/>
   * 
   * @param parseRequest  The parse request body object.
   * @return List of mentions of observation found in given text.
   */
  @CrossOrigin
  @ResponseBody
  @RequestMapping(value = "parse", method = RequestMethod.POST)
  public ResponseObject parse(@RequestBody InfermedicaParseRequest parseRequest) {
    LOGGER.info("Executing parse()");
    return infermedicaService.parse(parseRequest); 
  }
  
  /**
   * The getRiskFactors method returns a list of all available risk factors to the caller.<br/>
   * 
   * @param ID   The ID of the risk factor to retrieve.
   * @return  The list of risk factors.
   */
  @CrossOrigin
  @ResponseBody
  @RequestMapping(value = { "/risk_factors", "/risk_factors/{id}" }, method = RequestMethod.GET)
  public ResponseObject getRiskFactors(@PathVariable("id") Optional<String> ID) {
    LOGGER.info("Executing getRiskFactors()");
    return infermedicaService.getRiskFactors(ID.orElse(null)); 
  }
  
  /**
   * The getLabTests method returns a list of all available lab tests to the caller.<br/>
   * 
   * @param ID   The ID of the lab test to retrieve.
   * @return  The list of lab tests.
   */
  @CrossOrigin
  @ResponseBody
  @RequestMapping(value = { "/lab_tests", "/lab_tests/{id}" }, method = RequestMethod.GET)
  public ResponseObject getLabTests(@PathVariable("id") Optional<String> ID) {
    LOGGER.info("Executing getLabTests()");
    return infermedicaService.getLabTests(ID.orElse(null)); 
  }
  
  /**
   * The getSymptoms method returns a list of all available symptoms to the caller.<br/>
   * 
   * @param ID   The ID of the symptom to retrieve.
   * @return  The list of symptoms.
   */
  @CrossOrigin
  @ResponseBody
  @RequestMapping(value = { "/symptoms", "/symptoms/{id}" }, method = RequestMethod.GET)
  public ResponseObject getSymptoms(@PathVariable("id") Optional<String> ID) {
    LOGGER.info("Executing getSymptoms()");
    return infermedicaService.getSymptoms(ID.orElse(null)); 
  }
  
  /**
   * Returns list of observations matching the given phrase.<br/>
   * 
   * @return The list of observations.
   */
  @CrossOrigin
  @ResponseBody
  @RequestMapping(value = "/search", method = RequestMethod.GET)
  public ResponseObject search(@RequestParam(value = "phrase", required = true) String phrase,
      @RequestParam(value="sex", required = false) String sex,
      @RequestParam(value="max_results", required = false, defaultValue = "10") String maxresults
      ) {
    LOGGER.info("Executing search()");
    return infermedicaService.search(phrase, sex, maxresults); 
  }
  
  /**
   * Suggests possible symptoms based on provided patient information.<br/>
   * 
   * @param genericRequest  The generic request body object.
   * @return  The ResponseObject with the suggestions on possible symptoms based on provided patient information.
   */
  @CrossOrigin
  @ResponseBody
  @RequestMapping(value = "/suggest", method = RequestMethod.POST)
  public ResponseObject suggest(@RequestBody InfermedicaGenericRequest genericRequest) {
    LOGGER.info("Executing suggest()");
    return infermedicaService.suggest(genericRequest); 
  }
  
  /**
   * Estimates triage level based on provided patient information.<br/>
   * 
   * @param genericRequest  The generic request body object.
   * @return  The ResponseObject with the estimation triage level based on provided patient information.
   */
  @CrossOrigin
  @ResponseBody
  @RequestMapping(value = "/triage", method = RequestMethod.POST)
  public ResponseObject triage(@RequestBody InfermedicaGenericRequest genericRequest) {
    LOGGER.info("Executing triage()");
    return infermedicaService.triage(genericRequest); 
  }
}
