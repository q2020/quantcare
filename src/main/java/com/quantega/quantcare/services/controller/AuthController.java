package com.quantega.quantcare.services.controller;

import static com.quantega.quantcare.services.security.config.TokenAuthenticationFilter.AUTHORIZATION;
import static com.quantega.quantcare.services.security.config.TokenAuthenticationFilter.BEARER;
import static java.util.Optional.ofNullable;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.quantega.quantcare.services.dto.PasswordObject;
import com.quantega.quantcare.services.dto.Registration;
import com.quantega.quantcare.services.dto.User;
import com.quantega.quantcare.services.security.service.UserAuthenticationService;
import com.quantega.quantcare.services.service.AuthService;
import com.quantega.quantcare.services.service.dto.ResponseObject;

/*
 * My Inc Confidential
 * Class: AuthController.java
 * (C) Copyright My Inc. 2018
 * 
 * @author kk
 */
@RestController
public class AuthController {

	private final Logger LOGGER = LoggerFactory.getLogger(getClass());

	@Autowired
	private AuthService authService;

	@Autowired
	UserAuthenticationService userAuthenticationService;

	public AuthController() {
	}

	/**
	 * This is a test service, ping to check if the server is up and running.<br/>
	 * 
	 * @param request
	 *            The HttpServletRequest object received.
	 * @param response
	 *            The HttpServletResponse object received.
	 * @return The ping response in JSON Format
	 */
	@CrossOrigin
	@ResponseBody
	@RequestMapping(value = "/ping", method = RequestMethod.GET)
	public ResponseObject ping() {
		LOGGER.info("Executing ping()");
		ResponseObject responseObj = new ResponseObject(HttpStatus.OK.value());
		responseObj.setMessage("SUCCESS!!");
		return responseObj;
	}

	/**
	 * The register method is used for registering a new user with all the valid
	 * user information.<br/>
	 * The user's registration related information is passed as User object.<br/>
	 * This registration doesn't require any token to authenticate. <br/>
	 * <br/>
	 * 
	 * @param user
	 *            The User object received.
	 * @return The created user.
	 */
	@CrossOrigin
	@ResponseBody
	@RequestMapping(value = "/register", method = RequestMethod.POST)
	public ResponseObject register(@RequestBody Registration registration) {
		LOGGER.info("Executing register()");
		return authService.register(registration);
	}

	/**
	 * <br/>
	 * The user's login method. User provides credentials through request payload in
	 * JSON format. <br/>
	 * The authentication status is sent back to the user in response.<br/>
	 * This login dosn't require any token to authenticate user's session. <br/>
	 * <br/>
	 * 
	 * @param request
	 *            The HttpServletRequest object received.
	 * @param response
	 *            The HttpServletResponse object received.
	 * @param payload
	 *            The JSON payload received.
	 * @return The ResponseEntity on user's login status info in JSON Format
	 */
	@CrossOrigin
	@ResponseBody
	@RequestMapping(value = "/login", method = RequestMethod.POST)
	public ResponseObject login(@RequestBody User user) {
		LOGGER.info("Executing login()");
		String username = ofNullable(ofNullable(user.getUsername()).orElse(user.getEmail())).orElse(user.getMobile());
		String password = user.getPassword();
		return userAuthenticationService.login(username, password);
	}

	/**
	 * <br/>
	 * The logout method. User requests for logging out the active session. <br/>
	 * This login requires to provide the authentication token to logout. <br/>
	 * <br/>
	 * 
	 * @param token
	 *            The authentication token received to verify user's logon info.
	 * @param request
	 *            The HttpServletRequest object received.
	 * @param response
	 *            The HttpServletResponse object received.
	 * @param payload
	 *            The JSON payload received.
	 * @return The ResponseEntity on user's logout status info in JSON Format
	 */
	@CrossOrigin
	@ResponseBody
	@RequestMapping(value = "/logout", method = RequestMethod.POST)
	public ResponseObject logout(@RequestHeader(value = AUTHORIZATION) final String param) {
		LOGGER.info("Executing logout()");
		final String token = param.replace(BEARER, "").trim();
		return userAuthenticationService.logout(token);
	}

	/**
	 * <br/>
	 * The changepwd method is used for changing user's password with all the
	 * relevant information provided in the request.<br/>
	 * The relevant information along with old password for authentication is passed
	 * in the payload JSON.<br/>
	 * This changepwd requires a token to authenticate user's session. <br/>
	 * <br/>
	 * 
	 * @param token
	 *            The authentication token received to verify user's logon info.
	 * @param request
	 *            The HttpServletRequest object received.
	 * @param response
	 *            The HttpServletResponse object received.
	 * @param payload
	 *            The JSON payload received.
	 * @return The ResponseEntity on change password info in JSON Format
	 */
	@CrossOrigin
	@ResponseBody
	@RequestMapping(value = "/changepwd", method = RequestMethod.POST)
	public ResponseObject changePasswd(@RequestBody PasswordObject passwrdObj) {
		LOGGER.info("Executing changePasswd()");
		return authService.changePasswd(passwrdObj);
	}

	/**
	 * <br/>
	 * This method is related to resetting user's password. <br/>
	 * The user should provide the necessary information related to resetting
	 * password like security question and answer. <br/>
	 * <br/>
	 * 
	 * @param request
	 *            The HttpServletRequest object received.
	 * @param response
	 *            The HttpServletResponse object received.
	 * @param payload
	 *            The JSON payload received.
	 * @return The ResponseEntity on reset password status info in JSON Format
	 */
	@CrossOrigin
	@ResponseBody
	@RequestMapping(value = "/resetpwd", method = RequestMethod.POST)
	public ResponseObject resetPasswd(@RequestBody PasswordObject passwordObj) {
		LOGGER.info("Executing resetPasswd()");
		return authService.resetPasswd(passwordObj);
	}

  /**
   * <br/>
   * This method is used to resetting user's password via email. <br/>
   * The user should provide the necessary information related to resetting
   * password through email link provided. <br/>
   * <br/>
   * 
   * @param request
   *            The HttpServletRequest object received.
   * @param response
   *            The HttpServletResponse object received.
   * @param payload
   *            The JSON payload received.
   * @return The ResponseEntity on reset password status info in JSON Format
   */
  @CrossOrigin
  @ResponseBody
  @RequestMapping(value = "/resetviaemail", method = RequestMethod.POST)
  public ResponseObject resetPasswdViaEmail(@RequestBody PasswordObject passwordObj) {
    LOGGER.info("Executing resetPasswdViaEmail()");
    return authService.resetPasswdViaEmail(passwordObj);
  }

	/**
	 * <br/>
	 * The securityquestions method is used for retrieving all the security
	 * questions to be displayed.<br/>
	 * The list of questions is displayed and user chooses one one them for
	 * him/her.<br/>
	 * This securityquestions dosn't require any token to authenticate user's
	 * session. <br/>
	 * <br/>
	 * 
	 * @param request
	 *            The HttpServletRequest object received.
	 * @param response
	 *            The HttpServletResponse object received.
	 * @return The ResponseEntity on security questions info in JSON Format
	 */
	@CrossOrigin
	@ResponseBody
	@RequestMapping(value = "/securityquestions", method = RequestMethod.GET)
	public ResponseObject getSecurityQuestions() {
		LOGGER.info("Executing securityquestions()");
		return authService.getSecurityQuestions();
	}

	/**
	 * The securityquestion method is used for retrieving user's security
	 * question.<br/>
	 * The relevant information like username is passed in the payload JSON.<br/>
	 * This securityquestion requires a token to authenticate user's session. <br/>
	 * <br/>
	 * 
	 * @param request
	 *            The HttpServletRequest object received.
	 * @param response
	 *            The HttpServletResponse object received.
	 * @param payload
	 *            The JSON payload received.
	 * @return The ResponseEntity on security question info in JSON Format
	 */
	@CrossOrigin
	@ResponseBody
	@RequestMapping(value = "/securityquestion", method = RequestMethod.POST)
	public ResponseObject getSecurityQuestionForUser(@RequestBody User user) {
		LOGGER.info("Executing getSecurityQuestionForUser()");
		return authService.getSecurityQuestionForUser(user);
	}

  /**
   * The sendOTP method sends the OTP for the user.<br/>
   * <br/>
   * 
   * @param token
   *            The token to validate.
   * @param payload
   *            The JSON payload received.
   * @return The ResponseObject with the generated OTP.
   */
  @CrossOrigin
  @ResponseBody
  @RequestMapping(value = "/sendotp", method = RequestMethod.POST)
  public ResponseObject sendOTP(@RequestBody User user) {
    LOGGER.info("Executing sendOTP()");
    return authService.sendOTP(user);
  }

  /**
   * The resendOTP method resends the OTP for the user.<br/>
   * <br/>
   * 
   * @param token
   *            The token to validate.
   * @param payload
   *            The JSON payload received.
   * @return The ResponseObject with the generated OTP.
   */
  @CrossOrigin
  @ResponseBody
  @RequestMapping(value = "/resendotp", method = RequestMethod.POST)
  public ResponseObject resendOTP(@RequestBody User user) {
    LOGGER.info("Executing resendOTP()");
    return authService.resendOTP(user);
  }

	/**
	 * The matchOTP method validates the OTP for the user.<br/>
	 * <br/>
	 * 
	 * @param token
	 *            The token to validate.
	 * @param payload
	 *            The JSON payload received.
	 * @return The ResponseObject with the matching OTP information.
	 */
	@CrossOrigin
	@ResponseBody
	@RequestMapping(value = "/matchotp", method = RequestMethod.POST)
	public ResponseObject matchOTP(@RequestBody User user) {
		LOGGER.info("Executing matchOTP()");
		return authService.matchOTP(user);
	}

	@Value("")
	private String matchOTPUrl;
}
