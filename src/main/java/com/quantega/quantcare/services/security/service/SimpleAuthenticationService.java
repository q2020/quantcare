
package com.quantega.quantcare.services.security.service;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.quantega.quantcare.services.dto.Registration;
import com.quantega.quantcare.services.repositories.RegistrationRepository;
import com.quantega.quantcare.services.security.bean.User;
import com.quantega.quantcare.services.service.dto.ResponseObject;

@Service
final class SimpleAuthenticationService implements UserAuthenticationService {

	Map<String, User> users = new HashMap<>();

	/** LOGGER is issued for logging purposes */
	private static final Logger LOGGER = LoggerFactory.getLogger(SimpleAuthenticationService.class);

	@Autowired
	private RegistrationRepository registrationRepository;

	@Override
	public ResponseObject login(final String username, final String password) {

		LOGGER.info("Start of login()");

		String message = "unsuccessful!";
		ResponseObject response = new ResponseObject(HttpStatus.UNAUTHORIZED.value(), message);

		Registration registration = registrationRepository.findByEmailAndPasswordAndActiveOrMobileAndPasswordAndActive(
				username, password, true, username, password, true);
		if (registration != null) {

			registration.setPassword(null);

			String token = UUID.randomUUID().toString();

			final User user = new User(token, username, password);
			users.put(token, user);

			message = "successful!";
			response.setMessage(token);
			response.setData(registration);
			response.setStatus(HttpStatus.OK.value());
		}
		
	    LOGGER.info("End of login()");
	    
	    return response;
	}

	@Override
	public Optional<User> findByToken(final String token) {
		return Optional.ofNullable(users.get(token));
	}

	@Override
	public ResponseObject logout(final String token) {
		users.remove(token);
		return new ResponseObject(HttpStatus.OK.value(), "successful!");
		
	}
}
