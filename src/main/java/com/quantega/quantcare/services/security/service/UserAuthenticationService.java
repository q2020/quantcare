package com.quantega.quantcare.services.security.service;

import java.util.Optional;

import com.quantega.quantcare.services.security.bean.User;
import com.quantega.quantcare.services.service.dto.ResponseObject;

public interface UserAuthenticationService {

	/**
	 * Logs in with the given {@code username} and {@code password}.
	 *
	 * @param username
	 * @param password
	 * @return an {@link ResponseObject} of a user when login succeeds
	 */
	ResponseObject login(String username, String password);

	/**
	 * Finds a user by its dao-key.
	 *
	 * @param token
	 *            user dao key
	 * @return
	 */
	Optional<User> findByToken(String token);

	/**
	 * Logs out the given input {@code user}.
	 *
	 * @param user
	 *            the token of user to logout
	 * @return 
	 */
	ResponseObject logout(String token);
}