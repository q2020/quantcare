package com.quantega.quantcare.services.exception;

import java.io.Serializable;

/**
 * The customized exception for RCA module. <br/>
 * This exception class is an extension of RuntimeException. <br/>
 * So all the unchecked (as well as checked) exceptions can be handled using this exception.
 * 
 * @author kalyan
 * @version 1.0
 */
public class QuantegaException extends Exception implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * Empty constructor.
	 */
	public QuantegaException() {
		super();
	}

	/**
	 * Constructor with error message.
	 * 
	 * @param msg  The error message passed.
	 */
	public QuantegaException(String msg) {
		super(msg);
	}

	/**
   * Constructor with error message and the exception object. <br/>
   * 
   * @param msg  The error message passed.
	 * @param e    The exception object.
	 */
	public QuantegaException(String msg, Exception e) {
		super(msg, e);
	}

	/**
   * Constructor with the exception object. <br/>
   * 
   * @param e    The exception object.
	 */
	public QuantegaException(Exception e) {
		super(e);
	}

}