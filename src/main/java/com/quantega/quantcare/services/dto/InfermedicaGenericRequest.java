package com.quantega.quantcare.services.dto;

/*
 * My Inc Confidential
 * Class: InfermedicaGenericRequest.java
 * (C) Copyright My Inc. 2018
 * 
 * @author kk
 */
public class InfermedicaGenericRequest {

  private String sex;
  private int age;
  private Evidence evidence;
  private String extra;
  private String target;
  private String evaluated_at;
  
  /**
   * InfermedicaGenericRequest constructor.
   */
  public InfermedicaGenericRequest() {}
  
  /**
   * Getter for sex
   * 
   * @return the sex
   */
  public String getSex() {
    return sex;
  }

  /**
   * Setter for sex to set.
   *
   * @param sex the sex to set
   */
  public void setSex(String sex) {
    this.sex = sex;
  }

  /**
   * Getter for age
   * 
   * @return the age
   */
  public int getAge() {
    return age;
  }

  /**
   * Setter for age to set.
   *
   * @param age the age to set
   */
  public void setAge(int age) {
    this.age = age;
  }

  /**
   * Getter for evidence
   * 
   * @return the evidence
   */
  public Evidence getEvidence() {
    return evidence;
  }

  /**
   * Setter for evidence to set.
   *
   * @param evidence the evidence to set
   */
  public void setEvidence(Evidence evidence) {
    this.evidence = evidence;
  }

  /**
   * Getter for extra
   * 
   * @return the extra
   */
  public String getExtra() {
    return extra;
  }

  /**
   * Setter for extra to set.
   *
   * @param extra the extra to set
   */
  public void setExtra(String extra) {
    this.extra = extra;
  }

  /**
   * Getter for target
   * 
   * @return the target
   */
  public String getTarget() {
    return target;
  }

  /**
   * Setter for target to set.
   *
   * @param target the target to set
   */
  public void setTarget(String target) {
    this.target = target;
  }

  /**
   * Getter for evaluated_at
   * 
   * @return the evaluated_at
   */
  public String getEvaluated_at() {
    return evaluated_at;
  }

  /**
   * Setter for evaluated_at to set.
   *
   * @param evaluated_at the evaluated_at to set
   */
  public void setEvaluated_at(String evaluated_at) {
    this.evaluated_at = evaluated_at;
  }


  /*
   * Class: Evidence.java
   * 
   * @author kk
   */
  public class Evidence {
    private String id;
    private boolean initial;
    private boolean related;
    private String choice_id = "present";
    private String observed_at;

    /**
     * Evidence constructor.
     */
    public Evidence() {}

    /**
     * Getter for id
     * 
     * @return the id
     */
    public String getId() {
      return id;
    }

    /**
     * Setter for id to set.
     *
     * @param id the id to set
     */
    public void setId(String id) {
      this.id = id;
    }

    /**
     * Getter for initial
     * 
     * @return the initial
     */
    public boolean isInitial() {
      return initial;
    }

    /**
     * Setter for initial to set.
     *
     * @param initial the initial to set
     */
    public void setInitial(boolean initial) {
      this.initial = initial;
    }

    /**
     * Getter for related
     * 
     * @return the related
     */
    public boolean isRelated() {
      return related;
    }

    /**
     * Setter for related to set.
     *
     * @param related the related to set
     */
    public void setRelated(boolean related) {
      this.related = related;
    }

    /**
     * Getter for choice_id
     * 
     * @return the choice_id
     */
    public String getChoice_id() {
      return choice_id;
    }

    /**
     * Setter for choice_id to set.
     *
     * @param choice_id the choice_id to set
     */
    public void setChoice_id(String choice_id) {
      this.choice_id = choice_id;
    }

    /**
     * Getter for observed_at
     * 
     * @return the observed_at
     */
    public String getObserved_at() {
      return observed_at;
    }

    /**
     * Setter for observed_at to set.
     *
     * @param observed_at the observed_at to set
     */
    public void setObserved_at(String observed_at) {
      this.observed_at = observed_at;
    }
  }
}
