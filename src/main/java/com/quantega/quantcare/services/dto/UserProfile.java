package com.quantega.quantcare.services.dto;

import java.util.Date;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

/*
 * My Inc Confidential
 * Class: UserProfile.java
 * (C) Copyright My Inc. 2018
 * 
 * @author kk
 */
@Document(collection = "users")
public class UserProfile {

  @Id
  private String id;

  private String name;
  private String phoneNumber;
  private String email;
  private String gender;
  private Date dateOfBirth;
  private String bloodGroup;
  private String timezone;
  private String address1;
  private String address2;
  private String city;
  private String state;
  private String country;
  private String pincode;
  private String altContactNumber;
  private String language ;
  private byte[] photograph;
  private boolean active;
  
  private String countryCode;
  private String altCountryCode;
	
  public UserProfile() {}

  public UserProfile(String ID) {
    this.id = ID;
  }

  public UserProfile(String name, String phoneNumber, String email, String gender, 
      Date dateOfBirth, String bloodGroup, String timezone, String address1, String address2, 
      String city, String state, String country, String pincode, String altContactNumber, String language , 
      byte[] photograph) {
    
    this.name = name;
    this.phoneNumber = phoneNumber;
    this.email = email;
    this.gender = gender;
    this.dateOfBirth = dateOfBirth;
    this.bloodGroup = bloodGroup;
    this.timezone = timezone;
    this.address1 = address1;
    this.address2 = address2;
    this.city = city;
    this.state = state;
    this.country = country;
    this.pincode = pincode;
    this.altContactNumber = altContactNumber;
    this.language = language;
    this.photograph = photograph;

  }
  
  @Override
  public String toString() {
      return String.format(
              "User[email='%s'='%s'='%s']",
              email);
  }

  /**
   * Getter for id
   * 
   * @return the id
   */
  public String getId() {
    return id;
  }

  /**
   * Setter for id to set.
   *
   * @param id the id to set
   */
  public void setId(String id) {
    this.id = id;
  }

  /**
   * Getter for name
   * 
   * @return the name
   */
  public String getName() {
    return name;
  }

  /**
   * Setter for name to set.
   *
   * @param name the name to set
   */
  public void setName(String name) {
    this.name = name;
  }

  /**
   * Getter for phoneNumber
   * 
   * @return the phoneNumber
   */
  public String getPhoneNumber() {
    return phoneNumber;
  }

  /**
   * Setter for phoneNumber to set.
   *
   * @param phoneNumber the phoneNumber to set
   */
  public void setPhoneNumber(String phoneNumber) {
    this.phoneNumber = phoneNumber;
  }

  /**
   * Getter for email
   * 
   * @return the email
   */
  public String getEmail() {
    return email;
  }

  /**
   * Setter for email to set.
   *
   * @param email the email to set
   */
  public void setEmail(String email) {
    this.email = email;
  }

  /**
   * Getter for gender
   * 
   * @return the gender
   */
  public String getGender() {
    return gender;
  }

  /**
   * Setter for gender to set.
   *
   * @param gender the gender to set
   */
  public void setGender(String gender) {
    this.gender = gender;
  }

  /**
   * Getter for dateOfBirth
   * 
   * @return the dateOfBirth
   */
  public Date getDateOfBirth() {
    return dateOfBirth;
  }

  /**
   * Setter for dateOfBirth to set.
   *
   * @param dateOfBirth the dateOfBirth to set
   */
  public void setDateOfBirth(Date dateOfBirth) {
    this.dateOfBirth = dateOfBirth;
  }

  /**
   * Getter for bloodGroup
   * 
   * @return the bloodGroup
   */
  public String getBloodGroup() {
    return bloodGroup;
  }

  /**
   * Setter for bloodGroup to set.
   *
   * @param bloodGroup the bloodGroup to set
   */
  public void setBloodGroup(String bloodGroup) {
    this.bloodGroup = bloodGroup;
  }

  /**
   * Getter for timezone
   * 
   * @return the timezone
   */
  public String getTimezone() {
    return timezone;
  }

  /**
   * Setter for timezone to set.
   *
   * @param timezone the timezone to set
   */
  public void setTimezone(String timezone) {
    this.timezone = timezone;
  }

  /**
   * Getter for address1
   * 
   * @return the address1
   */
  public String getAddress1() {
    return address1;
  }

  /**
   * Setter for address1 to set.
   *
   * @param address1 the address1 to set
   */
  public void setAddress1(String address1) {
    this.address1 = address1;
  }

  /**
   * Getter for address2
   * 
   * @return the address2
   */
  public String getAddress2() {
    return address2;
  }

  /**
   * Setter for address2 to set.
   *
   * @param address2 the address2 to set
   */
  public void setAddress2(String address2) {
    this.address2 = address2;
  }

  /**
   * Getter for city
   * 
   * @return the city
   */
  public String getCity() {
    return city;
  }

  /**
   * Setter for city to set.
   *
   * @param city the city to set
   */
  public void setCity(String city) {
    this.city = city;
  }

  /**
   * Getter for state
   * 
   * @return the state
   */
  public String getState() {
    return state;
  }

  /**
   * Setter for state to set.
   *
   * @param state the state to set
   */
  public void setState(String state) {
    this.state = state;
  }

  /**
   * Getter for country
   * 
   * @return the country
   */
  public String getCountry() {
    return country;
  }

  /**
   * Setter for country to set.
   *
   * @param country the country to set
   */
  public void setCountry(String country) {
    this.country = country;
  }

  /**
   * Getter for pincode
   * 
   * @return the pincode
   */
  public String getPincode() {
    return pincode;
  }

  /**
   * Setter for pincode to set.
   *
   * @param pincode the pincode to set
   */
  public void setPincode(String pincode) {
    this.pincode = pincode;
  }

  /**
   * Getter for altContactNumber
   * 
   * @return the altContactNumber
   */
  public String getAltContactNumber() {
    return altContactNumber;
  }

  /**
   * Setter for altContactNumber to set.
   *
   * @param altContactNumber the altContactNumber to set
   */
  public void setAltContactNumber(String altContactNumber) {
    this.altContactNumber = altContactNumber;
  }

  /**
   * Getter for language
   * 
   * @return the language
   */
  public String getLanguage() {
    return language;
  }

  /**
   * Setter for language to set.
   *
   * @param language the language to set
   */
  public void setLanguage(String language) {
    this.language = language;
  }

  /**
   * Getter for photograph
   * 
   * @return the photograph
   */
  public byte[] getPhotograph() {
    return photograph;
  }

  /**
   * Setter for photograph to set.
   *
   * @param photograph the photograph to set
   */
  public void setPhotograph(byte[] photograph) {
    this.photograph = photograph;
  }

  /**
   * Getter for active
   * 
   * @return the active
   */
  public boolean isActive() {
    return active;
  }

  /**
   * Setter for active to set.
   *
   * @param active the active to set
   */
  public void setActive(boolean active) {
    this.active = active;
  }

	public String getCountryCode() {
		return countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	public String getAltCountryCode() {
		return altCountryCode;
	}

	public void setAltCountryCode(String altCountryCode) {
		this.altCountryCode = altCountryCode;
	}

}
