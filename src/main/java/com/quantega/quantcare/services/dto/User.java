package com.quantega.quantcare.services.dto;

/*
 * My Inc Confidential
 * Class: User.java
 * (C) Copyright My Inc. 2018
 * 
 * @author kk
 */
public class User {

	private String username;
	private String email;
	private String password;
	private String role;
	private String mobile;
	private short otp;
	private boolean loggedin;

	public User() {
	}

	public User(String email, String password) {
		this.email = email;
		this.password = password;
	}

	public User(String email, String password, String role) {
		this(email, password);
		this.role = role;
	}

	public User(String email, String password, String role, String mobile) {
		this(email, password, role);
		this.mobile = mobile;
	}

	public User(String email, String password, String role, String mobile, Short otp) {
		this(email, password, role, mobile);
		this.otp = otp;
	}

	/**
	 * Getter for email
	 * 
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * Getter for password
	 * 
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public void setOtp(short otp) {
		this.otp = otp;
	}

	/**
	 * Setter for password to set.
	 *
	 * @param password
	 *            the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * Getter for role
	 * 
	 * @return the role
	 */
	public String getRole() {
		return role;
	}

	/**
	 * Getter for mobile
	 * 
	 * @return the mobile
	 */
	public String getMobile() {
		return mobile;
	}

	/**
	 * Getter for otp
	 * 
	 * @return the otp
	 */
	public short getOtp() {
		return otp;
	}

	/**
	 * Getter for loggedin
	 * 
	 * @return the loggedin
	 */
	public boolean isLoggedin() {
		return loggedin;
	}

	/**
	 * Setter for loggedin to set.
	 *
	 * @param loggedin
	 *            the loggedin to set
	 */
	public void setLoggedin(boolean loggedin) {
		this.loggedin = loggedin;
	}

	@Override
	public String toString() {
		return String.format("User[email='%s', password='%s', role='%s']", email, password, role);
	}

}
