package com.quantega.quantcare.services.dto;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

/*
 * My Inc Confidential
 * Class: DoctorProfile.java
 * (C) Copyright My Inc. 2018
 * 
 * @author kk
 */
@Document(collection = "doctors")
public class DoctorProfile {

  @Id
  public String id;

  private String title;

  private String name;
  private String email;
  private String phoneNumber;
  private String countryCode;

  private String gender;

  private String city;
  private String country;

  private String language;
  private byte[] photograph;

  private String specializaion;

  private String degree;
  private String institute;
  private int completionYear;
  private String experience;

  private String registrationNumber;
  private String registrationCouncil;
  private int registrationYear;

  private String practice;
  private String clinicName;
  private String clinicCity;
  private String locality;

  private boolean active;
  
  /**
   * Getter for id
   * 
   * @return the id
   */
  public String getId() {
    return id;
  }

  /**
   * Setter for id to set.
   *
   * @param id the id to set
   */
  public void setId(String id) {
    this.id = id;
  }

  /**
   * Getter for title
   * 
   * @return the title
   */
  public String getTitle() {
    return title;
  }

  /**
   * Setter for title to set.
   *
   * @param title the title to set
   */
  public void setTitle(String title) {
    this.title = title;
  }

  /**
   * Getter for name
   * 
   * @return the name
   */
  public String getName() {
    return name;
  }

  /**
   * Setter for name to set.
   *
   * @param name the name to set
   */
  public void setName(String name) {
    this.name = name;
  }

  /**
   * Getter for email
   * 
   * @return the email
   */
  public String getEmail() {
    return email;
  }

  /**
   * Setter for email to set.
   *
   * @param email the email to set
   */
  public void setEmail(String email) {
    this.email = email;
  }

  /**
   * Getter for phoneNumber
   * 
   * @return the phoneNumber
   */
  public String getPhoneNumber() {
    return phoneNumber;
  }

  /**
   * Setter for phoneNumber to set.
   *
   * @param phoneNumber the phoneNumber to set
   */
  public void setPhoneNumber(String phoneNumber) {
    this.phoneNumber = phoneNumber;
  }

  /**
   * Getter for countryCode
   * 
   * @return the countryCode
   */
  public String getCountryCode() {
    return countryCode;
  }

  /**
   * Setter for countryCode to set.
   *
   * @param countryCode the countryCode to set
   */
  public void setCountryCode(String countryCode) {
    this.countryCode = countryCode;
  }

  /**
   * Getter for gender
   * 
   * @return the gender
   */
  public String getGender() {
    return gender;
  }

  /**
   * Setter for gender to set.
   *
   * @param gender the gender to set
   */
  public void setGender(String gender) {
    this.gender = gender;
  }

  /**
   * Getter for city
   * 
   * @return the city
   */
  public String getCity() {
    return city;
  }

  /**
   * Setter for city to set.
   *
   * @param city the city to set
   */
  public void setCity(String city) {
    this.city = city;
  }

  /**
   * Getter for country
   * 
   * @return the country
   */
  public String getCountry() {
    return country;
  }

  /**
   * Setter for country to set.
   *
   * @param country the country to set
   */
  public void setCountry(String country) {
    this.country = country;
  }

  /**
   * Getter for language
   * 
   * @return the language
   */
  public String getLanguage() {
    return language;
  }

  /**
   * Setter for language to set.
   *
   * @param language the language to set
   */
  public void setLanguage(String language) {
    this.language = language;
  }

  /**
   * Getter for photograph
   * 
   * @return the photograph
   */
  public byte[] getPhotograph() {
    return photograph;
  }

  /**
   * Setter for photograph to set.
   *
   * @param photograph the photograph to set
   */
  public void setPhotograph(byte[] photograph) {
    this.photograph = photograph;
  }

  /**
   * Getter for specializaion
   * 
   * @return the specializaion
   */
  public String getSpecializaion() {
    return specializaion;
  }

  /**
   * Setter for specializaion to set.
   *
   * @param specializaion the specializaion to set
   */
  public void setSpecializaion(String specializaion) {
    this.specializaion = specializaion;
  }

  /**
   * Getter for degree
   * 
   * @return the degree
   */
  public String getDegree() {
    return degree;
  }

  /**
   * Setter for degree to set.
   *
   * @param degree the degree to set
   */
  public void setDegree(String degree) {
    this.degree = degree;
  }

  /**
   * Getter for institute
   * 
   * @return the institute
   */
  public String getInstitute() {
    return institute;
  }

  /**
   * Setter for institute to set.
   *
   * @param institute the institute to set
   */
  public void setInstitute(String institute) {
    this.institute = institute;
  }

  /**
   * Getter for completionYear
   * 
   * @return the completionYear
   */
  public int getCompletionYear() {
    return completionYear;
  }

  /**
   * Setter for completionYear to set.
   *
   * @param completionYear the completionYear to set
   */
  public void setCompletionYear(int completionYear) {
    this.completionYear = completionYear;
  }

  /**
   * Getter for experience
   * 
   * @return the experience
   */
  public String getExperience() {
    return experience;
  }

  /**
   * Setter for experience to set.
   *
   * @param experience the experience to set
   */
  public void setExperience(String experience) {
    this.experience = experience;
  }

  /**
   * Getter for registrationNumber
   * 
   * @return the registrationNumber
   */
  public String getRegistrationNumber() {
    return registrationNumber;
  }

  /**
   * Setter for registrationNumber to set.
   *
   * @param registrationNumber the registrationNumber to set
   */
  public void setRegistrationNumber(String registrationNumber) {
    this.registrationNumber = registrationNumber;
  }

  /**
   * Getter for registrationCouncil
   * 
   * @return the registrationCouncil
   */
  public String getRegistrationCouncil() {
    return registrationCouncil;
  }

  /**
   * Setter for registrationCouncil to set.
   *
   * @param registrationCouncil the registrationCouncil to set
   */
  public void setRegistrationCouncil(String registrationCouncil) {
    this.registrationCouncil = registrationCouncil;
  }

  /**
   * Getter for registrationYear
   * 
   * @return the registrationYear
   */
  public int getRegistrationYear() {
    return registrationYear;
  }

  /**
   * Setter for registrationYear to set.
   *
   * @param registrationYear the registrationYear to set
   */
  public void setRegistrationYear(int registrationYear) {
    this.registrationYear = registrationYear;
  }

  /**
   * Getter for practice
   * 
   * @return the practice
   */
  public String getPractice() {
    return practice;
  }

  /**
   * Setter for practice to set.
   *
   * @param practice the practice to set
   */
  public void setPractice(String practice) {
    this.practice = practice;
  }

  /**
   * Getter for clinicName
   * 
   * @return the clinicName
   */
  public String getClinicName() {
    return clinicName;
  }

  /**
   * Setter for clinicName to set.
   *
   * @param clinicName the clinicName to set
   */
  public void setClinicName(String clinicName) {
    this.clinicName = clinicName;
  }

  /**
   * Getter for clinicCity
   * 
   * @return the clinicCity
   */
  public String getClinicCity() {
    return clinicCity;
  }

  /**
   * Setter for clinicCity to set.
   *
   * @param clinicCity the clinicCity to set
   */
  public void setClinicCity(String clinicCity) {
    this.clinicCity = clinicCity;
  }

  /**
   * Getter for locality
   * 
   * @return the locality
   */
  public String getLocality() {
    return locality;
  }

  /**
   * Setter for locality to set.
   *
   * @param locality the locality to set
   */
  public void setLocality(String locality) {
    this.locality = locality;
  }

  /**
   * Getter for active
   * 
   * @return the active
   */
  public boolean isActive() {
    return active;
  }

  /**
   * Setter for active to set.
   *
   * @param active the active to set
   */
  public void setActive(boolean active) {
    this.active = active;
  }

  public DoctorProfile() {}

  public DoctorProfile(String ID) {
    this.id = ID;
  }

  @Override
  public String toString() {
      return String.format(
              "Doctor[Id=%s, Name='%s', Email='%s']",
              id, name, email);
  }

}
