package com.quantega.quantcare.services.dto;

import java.util.Date;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

/*
 * My Inc Confidential
 * Class: Aadhaar.java
 * (C) Copyright My Inc. 2018
 * 
 * @author kk
 */
@Document(collection = "aadhaars")
public class Aadhaar {

  @Id
  private String id;

  private String repository;
  private String path;
  private boolean recursive;
  
  private String createdBy;
  private Date createdOn;
  private String lastModifiedBy;
  private Date lastModifiedOn;
  
  public Aadhaar() {}

  /**
   * Getter for repository
   * 
   * @return the repository
   */
  public String getRepository() {
    return repository;
  }

  /**
   * Setter for repository to set.
   *
   * @param repository the repository to set
   */
  public void setRepository(String repository) {
    this.repository = repository;
  }

  /**
   * Getter for path
   * 
   * @return the path
   */
  public String getPath() {
    return path;
  }

  /**
   * Setter for path to set.
   *
   * @param path the path to set
   */
  public void setPath(String path) {
    this.path = path;
  }

  /**
   * Getter for recursive
   * 
   * @return the recursive
   */
  public boolean isRecursive() {
    return recursive;
  }

  /**
   * Setter for recursive to set.
   *
   * @param recursive the recursive to set
   */
  public void setRecursive(boolean recursive) {
    this.recursive = recursive;
  }



  /**
   * Getter for createdBy
   * 
   * @return the createdBy
   */
  public String getCreatedBy() {
    return createdBy;
  }

  
  /**
   * Setter for createdBy to set.
   *
   * @param createdBy the createdBy to set
   */
  public void setCreatedBy(String createdBy) {
    this.createdBy = createdBy;
  }

  
  /**
   * Getter for createdOn
   * 
   * @return the createdOn
   */
  public Date getCreatedOn() {
    return createdOn;
  }

  
  /**
   * Setter for createdOn to set.
   *
   * @param createdOn the createdOn to set
   */
  public void setCreatedOn(Date createdOn) {
    this.createdOn = createdOn;
  }

  
  /**
   * Getter for lastModifiedBy
   * 
   * @return the lastModifiedBy
   */
  public String getLastModifiedBy() {
    return lastModifiedBy;
  }

  
  /**
   * Setter for lastModifiedBy to set.
   *
   * @param lastModifiedBy the lastModifiedBy to set
   */
  public void setLastModifiedBy(String lastModifiedBy) {
    this.lastModifiedBy = lastModifiedBy;
  }

  
  /**
   * Getter for lastModifiedOn
   * 
   * @return the lastModifiedOn
   */
  public Date getLastModifiedOn() {
    return lastModifiedOn;
  }

  
  /**
   * Setter for lastModifiedOn to set.
   *
   * @param lastModifiedOn the lastModifiedOn to set
   */
  public void setLastModifiedOn(Date lastModifiedOn) {
    this.lastModifiedOn = lastModifiedOn;
  }

  
  
  /**
   * Getter for recursive
   * 
   * @return the recursive
   */
  public boolean isActive() {
    return recursive;
  }

  
  /**
   * Setter for recursive to set.
   *
   * @param recursive the recursive to set
   */
  public void setActive(boolean active) {
    this.recursive = active;
  }

  @Override
  public String toString() {
    return String.format("CMSPage[id=%s, path='%s', repository='%s']", id, path, repository);
  }

}
