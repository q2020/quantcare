package com.quantega.quantcare.services.dto;

/*
 * My Inc Confidential
 * Class: IpGeoLocation.java
 * (C) Copyright My Inc. 2018
 * 
 * @author kk
 */

/**
 * IpGeoLocation class containing IP Address, Latitude and Longitude.
 */
public class IpGeoLocation {
  private String ip;
  private double latitude;
  private double longitude;
  private String city;
  private String subdivision;
  private String isoSubdivision;
  private String country;
  private String isoCountry;
  private String postalCode;
  
  /**
   * IpGeoLocation constructor with IP, Lat, Long and other attributes.
   * 
   * @param ip
   * @param latitude
   * @param longitude
   * @param city
   * @param subdivision
   * @param isoSubdivision
   * @param country
   * @param isoCountry
   * @param postalCode
   */
  public IpGeoLocation(String ip, double latitude, double longitude, 
      String city, 
      String subdivision, String isoSubdivision,
      String country, String isoCountry,
      String postalCode) {
    this.ip = ip;
    this.latitude = latitude;
    this.longitude = longitude;
    this.city = city;
    this.subdivision = subdivision;
    this.isoSubdivision = isoSubdivision;
    this.country = country;
    this.isoCountry = isoCountry;
    this.postalCode = postalCode;
  }
  
  @Override
  public String toString() {
    return "IP: " + ip + 
        ", Latitude: " + latitude + 
        ", Longitude: " + longitude + 
        ", City: " + city + 
        ", Subdivision: " + subdivision + 
        ", Country: " + country + 
        ", PostalCode: " + postalCode;
  }

  /**
   * Getter for ip
   * 
   * @return the ip
   */
  public String getIp() {
    return ip;
  }

  /**
   * Getter for latitude
   * 
   * @return the latitude
   */
  public double getLatitude() {
    return latitude;
  }

  /**
   * Getter for longitude
   * 
   * @return the longitude
   */
  public double getLongitude() {
    return longitude;
  }

  /**
   * Getter for country
   * 
   * @return the country
   */
  public String getCountry() {
    return country;
  }

  /**
   * Getter for isoCountry
   * 
   * @return the isoCountry
   */
  public String getIsoCountry() {
    return isoCountry;
  }

  /**
   * Getter for subdivision
   * 
   * @return the subdivision
   */
  public String getSubdivision() {
    return subdivision;
  }

  /**
   * Getter for isoSubdivision
   * 
   * @return the isoSubdivision
   */
  public String getIsoSubdivision() {
    return isoSubdivision;
  }

  /**
   * Getter for city
   * 
   * @return the city
   */
  public String getCity() {
    return city;
  }

  /**
   * Getter for postalCode
   * 
   * @return the postalCode
   */
  public String getPostalCode() {
    return postalCode;
  }
  
}
