package com.quantega.quantcare.services.dto;

/*
 * My Inc Confidential
 * Class: InfermedicaParseRequest.java
 * (C) Copyright My Inc. 2018
 * 
 * @author kk
 */
public class InfermedicaParseRequest {

  private String text;
  private String[] context;
  private boolean include_tokens;
  private boolean correct_spelling;
  private String[] concept_types;
  
  /**
   * InfermedicaParseRequest constructor.
   */
  public InfermedicaParseRequest() {}

  /**
   * Getter for text
   * 
   * @return the text
   */
  public String getText() {
    return text;
  }

  /**
   * Setter for text to set.
   *
   * @param text the text to set
   */
  public void setText(String text) {
    this.text = text;
  }

  /**
   * Getter for context
   * 
   * @return the context
   */
  public String[] getContext() {
    return context;
  }

  /**
   * Setter for context to set.
   *
   * @param context the context to set
   */
  public void setContext(String[] context) {
    this.context = context;
  }

  /**
   * Getter for include_tokens
   * 
   * @return the include_tokens
   */
  public boolean isInclude_tokens() {
    return include_tokens;
  }

  /**
   * Setter for include_tokens to set.
   *
   * @param include_tokens the include_tokens to set
   */
  public void setInclude_tokens(boolean include_tokens) {
    this.include_tokens = include_tokens;
  }

  /**
   * Getter for correct_spelling
   * 
   * @return the correct_spelling
   */
  public boolean isCorrect_spelling() {
    return correct_spelling;
  }

  /**
   * Setter for correct_spelling to set.
   *
   * @param correct_spelling the correct_spelling to set
   */
  public void setCorrect_spelling(boolean correct_spelling) {
    this.correct_spelling = correct_spelling;
  }

  /**
   * Getter for concept_types
   * 
   * @return the concept_types
   */
  public String[] getConcept_types() {
    return concept_types;
  }

  /**
   * Setter for concept_types to set.
   *
   * @param concept_types the concept_types to set
   */
  public void setConcept_types(String[] concept_types) {
    this.concept_types = concept_types;
  }
  
}
