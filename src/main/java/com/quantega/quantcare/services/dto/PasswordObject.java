package com.quantega.quantcare.services.dto;

/*
 * My Inc Confidential
 * Class: PasswordObject.java
 * (C) Copyright My Inc. 2018
 * 
 * @author kk
 */
public class PasswordObject {

  private String email;
  private String mobile;
  private String role;
  private String password;
  private String npassword;
  private String security;
  private String answer;

  public PasswordObject() {}

  public PasswordObject(String email, String role, String password, String npassword, String security, String answer) {
      this.email = email;
      this.role = role;
      this.password = password;
      this.npassword = npassword;
      this.security = security;
      this.answer = answer;
  }

  /**
   * Getter for email
   * 
   * @return the email
   */
  public String getEmail() {
    return email;
  }

  /**
   * Getter for mobile
   * 
   * @return the mobile
   */
  public String getMobile() {
    return mobile;
  }

  /**
   * Getter for password
   * 
   * @return the password
   */
  public String getPassword() {
    return password;
  }
  
  /**
   * Getter for role
   * 
   * @return the role
   */
  public String getRole() {
    return role;
  }

  /**
   * Setter for password to set.
   *
   * @param password the password to set
   */
  public void setPassword(String password) {
    this.password = password;
  }

  /**
   * Getter for npassword
   * 
   * @return the npassword
   */
  public String getNpassword() {
    return npassword;
  }

  
  /**
   * Setter for npassword to set.
   *
   * @param npassword the npassword to set
   */
  public void setNpassword(String npassword) {
    this.npassword = npassword;
  }

  /**
   * Getter for answer
   * 
   * @return the answer
   */
  public String getAnswer() {
    return answer;
  }

  /**
   * Getter for security
   * 
   * @return the security
   */
  public String getSecurity() {
    return security;
  }

  @Override
  public String toString() {
      return String.format(
              "Doctor[email='%s', password='%s', security='%s']",
              email, password, security);
  }

}
