package com.quantega.quantcare.services.dto;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

/*
 * My Inc Confidential
 * Class: SecurityQuestion.java
 * (C) Copyright My Inc. 2018
 * 
 * @author kk
 */
@Document(collection = "securityquestions")
public class SecurityQuestion {

  @Id
  private String id;

  private String question;
  private boolean active = true;

  public SecurityQuestion() {}

  public SecurityQuestion(String question) {
      this.question = question;
  }

  /**
   * Getter for question
   * 
   * @return the question
   */
  public String getQuestion() {
    return question;
  }

  /**
   * Getter for active
   * 
   * @return the active
   */
  public boolean isActive() {
    return active;
  }

  
  /**
   * Setter for active to set.
   *
   * @param active the active to set
   */
  public void setActive(boolean active) {
    this.active = active;
  }

  @Override
  public String toString() {
      return String.format(
              "SecurityQuestion[question='%s', active='%s']",
              question, active);
  }

}
