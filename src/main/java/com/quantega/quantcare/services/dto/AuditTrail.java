package com.quantega.quantcare.services.dto;

import java.util.Date;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

/*
 * My Inc Confidential
 * Class: AuditTrail.java
 * (C) Copyright My Inc. 2018
 * 
 * @author kk
 */
@Document(collection = "audittrail")
public class AuditTrail {

  @Id
  private String id;

  private String username;
  private String action;
  private Date accesstime;
  private String accessip;
  private String message;

  public AuditTrail(String username, String action, String accessip) {
      this.username = username;
      this.action = action;
      this.accessip = accessip;
      this.accesstime = new Date();
  }

  public AuditTrail(String username, String action, String accessip, Date accesstime, String message) {
    this.username = username;
    this.action = action;
    this.accessip = accessip;
    this.accesstime = accesstime;
    this.message = message;
  }

  /**
   * Getter for username
   * 
   * @return the username
   */
  public String getUsername() {
    return username;
  }

  /**
   * Getter for action
   * 
   * @return the action
   */
  public String getPassword() {
    return action;
  }
  
  /**
   * Setter for action to set.
   *
   * @param action the action to set
   */
  public void setPassword(String password) {
    this.action = password;
  }

  /**
   * Getter for accesstime
   * 
   * @return the accesstime
   */
  public Date getAccesstime() {
    return accesstime;
  }

  /**
   * Getter for accessip
   * 
   * @return the accessip
   */
  public String getAccessip() {
    return accessip;
  }

  /**
   * Getter for message
   * 
   * @return the message
   */
  public String getMessage() {
    return message;
  }

  
  /**
   * Setter for accessip to set.
   *
   * @param accessip the accessip to set
   */
  public void setAccessip(String accessip) {
    this.accessip = accessip;
  }

  @Override
  public String toString() {
      return String.format(
              "Doctor[username='%s', action='%s', accesstime='%s', accessip='%s']",
              username, action, accesstime, accessip);
  }

}
