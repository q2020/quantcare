package com.quantega.quantcare.services.dto;

import java.util.Date;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

/*
 * My Inc Confidential
 * Class: Registration.java
 * (C) Copyright My Inc. 2018
 * 
 * @author kk
 */
@Document(collection = "registrations")
public class Registration {

  @Id
  private String id;

  private String title;
  private String firstName;
  private String lastName;
  private String username;
  private String password;
  private String email;
  private String deviceType;
  private String security;
  private String answer;
  private String organization;
  private String country;
  private String mobile;
  private String userGroup;
  private String role;
  private String gender;
  private String createdBy;
  private Date createdOn;
  private String lastModifiedBy;
  private Date lastModifiedOn;
  private boolean active;
  private boolean offerConsent;
  private String countryCode;
  
  public Registration() {}

  /**
   * Getter for title
   * 
   * @return the title
   */
  public String getTitle() {
    return title;
  }

  
  /**
   * Setter for title to set.
   *
   * @param title the title to set
   */
  public void setTitle(String title) {
    this.title = title;
  }

  
  /**
   * Getter for firstName
   * 
   * @return the firstName
   */
  public String getFirstName() {
    return firstName;
  }

  
  /**
   * Setter for firstName to set.
   *
   * @param firstName the firstName to set
   */
  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }

  
  /**
   * Getter for lastName
   * 
   * @return the lastName
   */
  public String getLastName() {
    return lastName;
  }

  
  /**
   * Setter for lastName to set.
   *
   * @param lastName the lastName to set
   */
  public void setLastName(String lastName) {
    this.lastName = lastName;
  }

  
  /**
   * Getter for username
   * 
   * @return the username
   */
  public String getUsername() {
    return username;
  }

  
  /**
   * Setter for username to set.
   *
   * @param username the username to set
   */
  public void setUsername(String username) {
    this.username = username;
  }

  
  /**
   * Getter for password
   * 
   * @return the password
   */
  public String getPassword() {
    return password;
  }

  
  /**
   * Setter for password to set.
   *
   * @param password the password to set
   */
  public void setPassword(String password) {
    this.password = password;
  }

  
  /**
   * Getter for email
   * 
   * @return the email
   */
  public String getEmail() {
    return email;
  }

  
  /**
   * Setter for email to set.
   *
   * @param email the email to set
   */
  public void setEmail(String email) {
    this.email = email;
  }

  
  /**
   * Getter for deviceType
   * 
   * @return the deviceType
   */
  public String getDeviceType() {
    return deviceType;
  }

  
  /**
   * Setter for deviceType to set.
   *
   * @param deviceType the deviceType to set
   */
  public void setDeviceType(String deviceType) {
    this.deviceType = deviceType;
  }

  
  /**
   * Getter for security
   * 
   * @return the security
   */
  public String getSecurity() {
    return security;
  }

  
  /**
   * Setter for security to set.
   *
   * @param security the security to set
   */
  public void setSecurity(String security) {
    this.security = security;
  }

  
  /**
   * Getter for answer
   * 
   * @return the answer
   */
  public String getAnswer() {
    return answer;
  }

  
  /**
   * Setter for answer to set.
   *
   * @param answer the answer to set
   */
  public void setAnswer(String answer) {
    this.answer = answer;
  }

  
  /**
   * Getter for organization
   * 
   * @return the organization
   */
  public String getOrganization() {
    return organization;
  }

  
  /**
   * Setter for organization to set.
   *
   * @param organization the organization to set
   */
  public void setOrganization(String organization) {
    this.organization = organization;
  }

  
  /**
   * Getter for country
   * 
   * @return the country
   */
  public String getCountry() {
    return country;
  }

  
  /**
   * Setter for country to set.
   *
   * @param country the country to set
   */
  public void setCountry(String country) {
    this.country = country;
  }

  
  /**
   * Getter for mobile
   * 
   * @return the mobile
   */
  public String getMobile() {
    return mobile;
  }

  
  /**
   * Setter for mobile to set.
   *
   * @param mobile the mobile to set
   */
  public void setMobile(String mobile) {
    this.mobile = mobile;
  }

  
  /**
   * Getter for userGroup
   * 
   * @return the userGroup
   */
  public String getUserGroup() {
    return userGroup;
  }

  
  /**
   * Setter for userGroup to set.
   *
   * @param userGroup the userGroup to set
   */
  public void setUserGroup(String userGroup) {
    this.userGroup = userGroup;
  }

  
  /**
   * Getter for role
   * 
   * @return the role
   */
  public String getRole() {
    return role;
  }

  
  /**
   * Setter for role to set.
   *
   * @param role the role to set
   */
  public void setRole(String role) {
    this.role = role;
  }

  
  /**
   * Getter for gender
   * 
   * @return the gender
   */
  public String getGender() {
    return gender;
  }

  
  /**
   * Setter for gender to set.
   *
   * @param gender the gender to set
   */
  public void setGender(String gender) {
    this.gender = gender;
  }

  
  /**
   * Getter for createdBy
   * 
   * @return the createdBy
   */
  public String getCreatedBy() {
    return createdBy;
  }

  
  /**
   * Setter for createdBy to set.
   *
   * @param createdBy the createdBy to set
   */
  public void setCreatedBy(String createdBy) {
    this.createdBy = createdBy;
  }

  
  /**
   * Getter for createdOn
   * 
   * @return the createdOn
   */
  public Date getCreatedOn() {
    return createdOn;
  }

  
  /**
   * Setter for createdOn to set.
   *
   * @param createdOn the createdOn to set
   */
  public void setCreatedOn(Date createdOn) {
    this.createdOn = createdOn;
  }

  
  /**
   * Getter for lastModifiedBy
   * 
   * @return the lastModifiedBy
   */
  public String getLastModifiedBy() {
    return lastModifiedBy;
  }

  
  /**
   * Setter for lastModifiedBy to set.
   *
   * @param lastModifiedBy the lastModifiedBy to set
   */
  public void setLastModifiedBy(String lastModifiedBy) {
    this.lastModifiedBy = lastModifiedBy;
  }

  
  /**
   * Getter for lastModifiedOn
   * 
   * @return the lastModifiedOn
   */
  public Date getLastModifiedOn() {
    return lastModifiedOn;
  }

  
  /**
   * Setter for lastModifiedOn to set.
   *
   * @param lastModifiedOn the lastModifiedOn to set
   */
  public void setLastModifiedOn(Date lastModifiedOn) {
    this.lastModifiedOn = lastModifiedOn;
  }

  
  
  /**
   * Getter for active
   * 
   * @return the active
   */
  public boolean isActive() {
    return active;
  }

  
  /**
   * Setter for active to set.
   *
   * @param active the active to set
   */
  public void setActive(boolean active) {
    this.active = active;
  }

  @Override
  public String toString() {
    return String.format("Registration[id=%s, firstName='%s', lastName='%s']", id, firstName, lastName);
  }

	public boolean isOfferConsent() {
		return offerConsent;
	}

	public void setOfferConsent(boolean offerConsent) {
		this.offerConsent = offerConsent;
	}

	public String getCountryCode() {
		return countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

}
