package com.quantega.quantcare.services.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.quantega.quantcare.services.dto.CMSPage;
import com.quantega.quantcare.services.exception.QuantegaException;
import com.quantega.quantcare.services.repositories.CMSRepository;
import com.quantega.quantcare.services.service.dto.ResponseObject;

/**
 * This class receives all the delegated services from CMSController.
 * So all the requests related to CMS are delegated here from the controller.<br/>
 * This includes page creation, edit a page and even deletion. <br/>
 * 
 * @author kalyan
 * @version 1.0
 */
@Service("cmsService")
public final class CMSService extends AbstractRestService implements IRestService {

  /** LOGGER is issued for logging purposes */
  private static final Logger LOGGER = LoggerFactory.getLogger(CMSService.class);

  @Autowired
  private CMSRepository cmsRepository;
  
  @Value("${magnolia.cms.url}")
  private String magnoliaCmsUrl;
  

  /**
   * CMS service constructor.
   */
  public CMSService() {}

  /**
   * The register method is used for registering a new user with all the valid user information.<br/>
   * The user's registration related information is passed in the User object.<br/>
   * This registration doesn't require any token to authenticate. <br/>
   * <br/>
   * 
   * @param page   The page to create.
   * @return  The created page.
   */
  public ResponseObject create(CMSPage page) {
    
    LOGGER.info("Start of create()");

    String publishUrl = magnoliaCmsUrl + MAGNOLIA_PUBLISH_URL;

    String urlParameters = "repository=%s&path=%s&recursive=%s";
    
    String repository = page.getRepository();
    String path = page.getPath();
    boolean recursive = page.isRecursive();
    
    String action = "publish";
    String message = UNSUCCESSFUL;

    ResponseObject response =  null;
    try {
      response = post(publishUrl, String.format(urlParameters, repository, path, recursive));
    } catch (QuantegaException e) {
      LOGGER.error(e.getMessage(), e);
    }
    
    message = SUCCESSFUL;
    response.setStatus(HttpStatus.OK.value());
    response.setMessage(message);
    
    saveAuditTrail(path, action, "0.0.0.0", message);
    LOGGER.info("End of create()");
    
    return response;
  }
  
  /**
   * The edit method edits an existing page.<br/>
   * The page content is received in REST call and saved back to the CMS through the CMS provided API calls<br/>
   * <br/>
   * 
   * @param user   The page to edit.
   * @return  The edited page.
   */
  @CrossOrigin
  @ResponseBody
  @RequestMapping(value = "/edit", method = RequestMethod.POST)
  public ResponseObject edit(CMSPage page) {
    
    LOGGER.info("Start of edit()");

    String publishUrl = magnoliaCmsUrl + MAGNOLIA_PUBLISH_URL;

    String urlParameters = "repository=%s&path=%s&recursive=%s";
    
    String repository = page.getRepository();
    String path = page.getPath();
    boolean recursive = page.isRecursive();
    
    String action = "publish";
    String message = UNSUCCESSFUL;

    ResponseObject response =  null;
    try {
      response = post(publishUrl, String.format(urlParameters, repository, path, recursive));
    } catch (QuantegaException e) {
      LOGGER.error(e.getMessage(), e);
    }
    
    message = SUCCESSFUL;
    response.setStatus(HttpStatus.OK.value());
    response.setMessage(message);
    
    saveAuditTrail(path, action, "0.0.0.0", message);
    LOGGER.info("End of edit()");
    
    return response;
  }

  /**
   * The publish method publishes the page just created.<br/>
   * The page publish is received in REST call and publishes the CMS through the CMS provided API calls<br/>
   * <br/>
   * 
   * @param page   The page to publish.
   * @return  The published page.
   */
  public ResponseObject publish(CMSPage page) {
    
    LOGGER.info("Start of publish()");
    
    String publishUrl = magnoliaCmsUrl + MAGNOLIA_PUBLISH_URL;

    String urlParameters = "repository=%s&path=%s&recursive=%s";
    
    String repository = page.getRepository();
    String path = page.getPath();
    boolean recursive = page.isRecursive();
    
    String action = "publish";
    String message = UNSUCCESSFUL;

    ResponseObject response =  null;
    try {
      response = post(publishUrl, String.format(urlParameters, repository, path, recursive));
    } catch (QuantegaException e) {
      LOGGER.error(e.getMessage(), e);
    }

    saveAuditTrail(path, action, "0.0.0.0", message);
    LOGGER.info("End of publish()");
    
    return response;
  }

  /**
   * The unpublish method publishes the page created.<br/>
   * The page unpublish is received in REST call and unpublishes the CMS through the CMS provided API calls<br/>
   * <br/>
   * 
   * @param page   The page to publish.
   * @return  The published page.
   */
  public ResponseObject unpublish(CMSPage page) {
    
    LOGGER.info("Start of unpublish()");
    
    String publishUrl = magnoliaCmsUrl + MAGNOLIA_UNPUBLISH_URL;

    String urlParameters = "repository=%s&path=%s&recursive=%s";
    
    String repository = page.getRepository();
    String path = page.getPath();
    boolean recursive = page.isRecursive();
    
    String action = "unpublish";
    String message = UNSUCCESSFUL;

    ResponseObject response =  null;
    try {
      response = post(publishUrl, String.format(urlParameters, repository, path, recursive));
    } catch (QuantegaException e) {
      LOGGER.error(e.getMessage(), e);
    }

    saveAuditTrail(path, action, "0.0.0.0", message);
    LOGGER.info("End of unpublish()");
    
    return response;
  }

  /**
   * The delete method delete an existing page.<br/>
   * The page content is received in REST call and deleted from the CMS through the CMS provided API calls<br/>
   * <br/>
   * 
   * @param user   The page to delete.
   * @return  The deleted page.
   */
  public ResponseObject delete(CMSPage page) {
    
    LOGGER.info("Start of delete()");

    String publishUrl = magnoliaCmsUrl + MAGNOLIA_PUBLISH_URL;

    String urlParameters = "repository=%s&path=%s&recursive=%s";
    
    String repository = page.getRepository();
    String path = page.getPath();
    boolean recursive = page.isRecursive();
    
    String action = "publish";
    String message = UNSUCCESSFUL;

    ResponseObject response =  null;
    try {
      response = post(publishUrl, String.format(urlParameters, repository, path, recursive));
    } catch (QuantegaException e) {
      LOGGER.error(e.getMessage(), e);
    }
    
    message = SUCCESSFUL;
    response.setStatus(HttpStatus.OK.value());
    response.setMessage(message);
    
    saveAuditTrail(path, action, "0.0.0.0", message);
    LOGGER.info("End of delete()");
    
    return response;
  }
}
