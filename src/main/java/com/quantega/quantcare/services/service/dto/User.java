/**
 * 
 */
package com.quantega.quantcare.services.service.dto;

import java.util.Date;

/*
 * Quantega Confidential
 * Class: User.java
 * (C) Copyright Quantega Inc. 2018
 */

/**
 * @author kalyan
 *
 */
public class User {
  
  protected String userId; 
  protected String userName; 
  protected String emailAddress; 
  protected String loginName;
  protected String fullDisplayName;
  protected String createdBy;
  protected Date createdDt;
  protected String updatedBy;
  protected Date updatedDt;
  
  protected int sortFieldInd;
  /**
   * Construct a LDAP User object and set the Sort Field Indicator.
   * 
   * @param sortFieldInd the sort field ind
   */
  public User(int sortFieldInd)
  {
    this.sortFieldInd = sortFieldInd;
  }

  /**
   * @param userId
   * @param userName
   * @param emailAddress
   * @param loginName
   */
  public User(String userId, String userName, String emailAddress, String loginName) {
    // TODO Auto-generated constructor stub
  }

  /**
   * @param userId
   * @param userName
   * @param emailAddress
   * @param loginName
   * @param fullDisplayName
   * @param createdBy
   * @param createdDt
   * @param updatedBy
   * @param updatedDt
   */
  public User(String userId, String userName, String emailAddress, String loginName, String fullDisplayName,
      String createdBy, Date createdDt, String updatedBy, Date updatedDt) {
    // TODO Auto-generated constructor stub
  }

}
