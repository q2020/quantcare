package com.quantega.quantcare.services.service;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.quantega.quantcare.services.dto.DoctorProfile;
import com.quantega.quantcare.services.dto.PasswordObject;
import com.quantega.quantcare.services.dto.Registration;
import com.quantega.quantcare.services.dto.SecurityQuestion;
import com.quantega.quantcare.services.dto.User;
import com.quantega.quantcare.services.dto.UserProfile;
import com.quantega.quantcare.services.exception.QuantegaException;
import com.quantega.quantcare.services.repositories.DoctorProfileRepository;
import com.quantega.quantcare.services.repositories.RegistrationRepository;
import com.quantega.quantcare.services.repositories.SecurityQuestionRepository;
import com.quantega.quantcare.services.repositories.UserProfileRepository;
import com.quantega.quantcare.services.service.dto.ResponseObject;

/**
 * This class receives all the delegated services from AuthController. So all
 * the requests related to authentication are delegated here from the
 * controller.<br/>
 * This includes user's registration to user's login/logout <br/>
 * It also handles a few more authentication services like security
 * question/answer, resetting password etc.
 * 
 * @author kalyan
 * @version 1.0
 */
@Service("authService")
public final class AuthService extends AbstractRestService implements IRestService {

	/** LOGGER is issued for logging purposes */
	private static final Logger LOGGER = LoggerFactory.getLogger(AuthService.class);

	@Autowired
	private RegistrationRepository registrationRepository;

	@Autowired
	private SecurityQuestionRepository securityQuestionRepository;

	@Autowired
	private DoctorProfileRepository doctorProfileRepository;

	@Autowired
	private UserProfileRepository userProfileRepository;

	@Value("${sms.senderid}")
	private String SENDERID;

	@Value("${sms.authkey}")
	private String AUTHKEY;

	@Value("${sms.sendotp.url}")
	private String sendotpUrl;

	@Value("${sms.matchotp.url}")
	private String matchotpUrl;

	/**
	 * Authentication service constructor.
	 */
	public AuthService() {
	}

	/**
	 * The register method is used for registering a new user with all the valid
	 * user information.<br/>
	 * The user's registration related information is passed in the User
	 * object.<br/>
	 * This registration doesn't require any token to authenticate. <br/>
	 * <br/>
	 * 
	 * @param user
	 *            The to create.
	 * @return The created user.
	 */
	public ResponseObject register(Registration registration) {

		LOGGER.info("Start of register()");

		String email = registration.getEmail();
		String mobile = registration.getMobile();
		String action = "registration";
		String message = "unsuccessful!";

		ResponseObject response = new ResponseObject(HttpStatus.CONFLICT.value(), message);

		if (registrationRepository.findByEmailOrMobile(email, mobile) != null) {
			message = email + ": User already exists!";
			response.setMessage(message);
			saveAuditTrail(email, action, "0.0.0.0", message);
			return response;
		}

		try {
			registration.setPassword(md5encrypt(decrypt(registration.getPassword())));
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		registration.setCreatedBy("admin");
		registration.setCreatedOn(new Date());
		registration.setLastModifiedBy("admin");
		registration.setLastModifiedOn(new Date());
		registration.setActive(true);

		registrationRepository.save(registration);

		String userGroup = registration.getUserGroup();
		String fullName = (registration.getFirstName() == null ? "" : (registration.getFirstName() + " "))
				+ registration.getLastName();
		if (DOCTOR.equals(userGroup)) {
			DoctorProfile doctorProfile = new DoctorProfile();
			doctorProfile.setName(fullName);
			doctorProfile.setEmail(registration.getEmail());
			doctorProfile.setPhoneNumber(registration.getMobile());
			doctorProfile.setGender(registration.getGender());
			doctorProfile.setCountry(registration.getCountry());
			doctorProfile.setCountryCode(registration.getCountryCode());
			doctorProfile.setActive(true);
			doctorProfileRepository.save(doctorProfile);
		} else if (USER.equals(userGroup)) {
			UserProfile userProfile = new UserProfile();
			userProfile.setName(fullName);
			userProfile.setEmail(registration.getEmail());
			userProfile.setPhoneNumber(registration.getMobile());
			userProfile.setGender(registration.getGender());
			userProfile.setCountry(registration.getCountry());
			userProfile.setCountryCode(registration.getCountryCode());
			userProfile.setActive(true);
			userProfileRepository.save(userProfile);
		}

		message = "successful!";

		response.setStatus(HttpStatus.OK.value());
		response.setMessage(message);

		saveAuditTrail(email, action, "0.0.0.0", message);
		LOGGER.info("End of register()");

		return response;
	}

	/**
	 * <br/>
	 * The user's login method. User provides credentials through request payload in
	 * JSON format. <br/>
	 * The authentication status is sent back to the user in response.<br/>
	 * This login dosn't require any token to authenticate user's session. <br/>
	 * <br/>
	 * 
	 * @param request
	 *            The HttpServletRequest object received.
	 * @param response
	 *            The HttpServletResponse object received.
	 * @param payload
	 *            The JSON payload received.
	 * @return The ResponseEntity on user's login status info in JSON Format
	 */
	public ResponseObject login(User user) {
		LOGGER.info("Start of login()");
		String email = user.getEmail();
		String mobile = user.getMobile();
		String password = user.getPassword();

		String action = "login";
		String message = "unsuccessful!";

		ResponseObject response = new ResponseObject(HttpStatus.UNAUTHORIZED.value(), message);

		Registration registration = registrationRepository.findByEmailAndPasswordAndActiveOrMobileAndPasswordAndActive(
				email, password, true, mobile, password, true);

		if (registration != null) {

			registration.setPassword(null);

			String token = UUID.randomUUID().toString();
			TOKENUSERMAP.put(token, registration);
			USERTOKENMAP.put(registration.getEmail(), token);
			USERTOKENMAP.put(registration.getMobile(), token);

			message = "successful!";

			response.setMessage(token);
			response.setData(registration);
			response.setStatus(HttpStatus.OK.value());
		}

		saveAuditTrail(email, action, "0.0.0.0", message);
		LOGGER.info("End of login()");

		return response;
	}

	/**
	 * The logout method to end the user's session. User requests for logging out
	 * the active session. <br/>
	 * This login requires to provide the authentication token to logout. <br/>
	 * <br/>
	 * 
	 * @param token
	 *            The authentication token received to verify user's logon info.
	 * @param request
	 *            The HttpServletRequest object received.
	 * @param response
	 *            The HttpServletResponse object received.
	 * @param payload
	 *            The JSON payload received.
	 * @return The ResponseEntity on user's logout status info in JSON Format
	 */
	public ResponseObject logout(String token) {

		LOGGER.info("Start of logout()");
		ResponseObject response = new ResponseObject(HttpStatus.OK.value());

		String action = "logout";
		String message = "successful!";

		Registration registration = TOKENUSERMAP.get(token);

		USERTOKENMAP.remove(registration.getEmail());
		USERTOKENMAP.remove(registration.getMobile());
		TOKENUSERMAP.remove(token);

		// releaseIdleSessions();

		saveAuditTrail(registration.getEmail(), action, "0.0.0.0", message);
		LOGGER.info("End of logout()");
		return response;
	}

	/**
	 * The changepwd method is used for changing user's password with all the
	 * relevant information provided in the request.<br/>
	 * The relevant information along with old password for authentication is passed
	 * in the payload JSON.<br/>
	 * This changepwd requires a token to authenticate user's session. <br/>
	 * <br/>
	 * 
	 * @param token
	 *            The authentication token received to verify user's logon info.
	 * @param passwdObj
	 *            The HttpServletRequest object received.
	 * @return The ResponseEntity on change password info in JSON Format
	 */
	public ResponseObject changePasswd(PasswordObject passwdObj) {
		LOGGER.info("Start of changePasswd()");
		ResponseObject response = new ResponseObject(HttpStatus.OK.value());

		String email = passwdObj.getEmail();
		String mobile = passwdObj.getMobile();
		String password = passwdObj.getPassword();

		String action = "changePasswd";
		String message = "unsuccessful!";

		Registration registration = registrationRepository.findByEmailAndPasswordAndActiveOrMobileAndPasswordAndActive(
				email, password, true, mobile, password, true);

		if (registration == null) {
			response.setMessage(message);
			saveAuditTrail(email, action, "0.0.0.0", message);

			response.setStatus(HttpStatus.NOT_ACCEPTABLE.value());
			response.setError(email + ": user doesn't exist or disabled!");
			return response;
		}

		registration.setPassword(passwdObj.getNpassword());
		registration.setLastModifiedBy("admin");
		registration.setLastModifiedOn(new Date());
		registration.setActive(true);

		registrationRepository.save(registration);

		message = "successful!";
		saveAuditTrail(email, action, "0.0.0.0", message);

		LOGGER.info("End of changePasswd()");
		return response;
	}

	/**
	 * This method is related to resetting user's password. <br/>
	 * The user should provide the necessary information related to resetting
	 * password like security question and answer. <br/>
	 * <br/>
	 * 
	 * @param request
	 *            The HttpServletRequest object received.
	 * @param response
	 *            The HttpServletResponse object received.
	 * @param payload
	 *            The JSON payload received.
	 * @return The ResponseEntity on reset password status info in JSON Format
	 */
	public ResponseObject resetPasswd(PasswordObject passwordObj) {

		LOGGER.info("Start of resetPasswd()");
		ResponseObject response = new ResponseObject(HttpStatus.OK.value());

		String email = passwordObj.getEmail();
		String mobile = passwordObj.getMobile();
		String security = passwordObj.getSecurity();
		String answer = passwordObj.getAnswer();
		Registration registration = registrationRepository
				.findByEmailAndSecurityAndAnswerAndActiveOrMobileAndSecurityAndAnswerAndActive(email, security, answer,
						true, mobile, security, answer, true);

		String action = "resetPasswd";
		String message = "unsuccessful!";

		if (registration == null) {
			response.setMessage(message);
			response.setStatus(HttpStatus.UNAUTHORIZED.value());
			saveAuditTrail(email, action, "0.0.0.0", message);
			return response;
		}

		registration.setPassword(passwordObj.getNpassword());

		registrationRepository.save(registration);

		message = "successful!";
		saveAuditTrail(email, action, "0.0.0.0", message);

		response.setMessage(message);
		LOGGER.info("End of resetPasswd()");
		return response;

	}

  /**
   * This method is used to resetting user's password. <br/>
   * The user should provide the necessary information related to resetting
   * password through email. <br/>
   * <br/>
   * 
   * @param request
   *            The HttpServletRequest object received.
   * @param response
   *            The HttpServletResponse object received.
   * @param payload
   *            The JSON payload received.
   * @return The ResponseEntity on reset password status info in JSON Format
   */
  public ResponseObject resetPasswdViaEmail(PasswordObject passwordObj) {

    LOGGER.info("Start of resetPasswdViaEmail()");
    ResponseObject response = new ResponseObject(HttpStatus.OK.value());

    String email = passwordObj.getEmail();
    String mobile = passwordObj.getMobile();
    String security = passwordObj.getSecurity();
    String answer = passwordObj.getAnswer();
    Registration registration = registrationRepository
        .findByEmailAndSecurityAndAnswerAndActiveOrMobileAndSecurityAndAnswerAndActive(email, security, answer,
            true, mobile, security, answer, true);

    String action = "resetPasswd";
    String message = "unsuccessful!";

    if (registration == null) {
      response.setMessage(message);
      response.setStatus(HttpStatus.UNAUTHORIZED.value());
      saveAuditTrail(email, action, "0.0.0.0", message);
      return response;
    }

    registration.setPassword(passwordObj.getPassword());

    registrationRepository.save(registration);

    message = "successful!";
    saveAuditTrail(email, action, "0.0.0.0", message);

    response.setMessage(message);
    LOGGER.info("End of resetPasswdViaEmail()");
    return response;

  }

	/**
	 * The securityquestions method is used for retrieving all the security
	 * questions to be displayed.<br/>
	 * The list of questions is displayed and user chooses one one them for
	 * him/her.<br/>
	 * This securityquestions dosn't require any token to authenticate user's
	 * session. <br/>
	 * <br/>
	 * 
	 * @param request
	 *            The HttpServletRequest object received.
	 * @param response
	 *            The HttpServletResponse object received.
	 * @return The ResponseEntity on security questions info in JSON Format
	 */
	public ResponseObject getSecurityQuestions() {
		LOGGER.info("Start of getSecurityQuestions()");
		ResponseObject response = new ResponseObject(HttpStatus.OK.value());
		List<SecurityQuestion> questions = securityQuestionRepository.findByActive(true);
		response.setData(questions);
		return response;
	}

	/**
	 * The securityquestion method is used for retrieving user's security
	 * question.<br/>
	 * The relevant information like email is passed in the payload JSON.<br/>
	 * This securityquestion requires a token to authenticate user's session. <br/>
	 * <br/>
	 * 
	 * @param request
	 *            The HttpServletRequest object received.
	 * @param response
	 *            The HttpServletResponse object received.
	 * @param payload
	 *            The JSON payload received.
	 * @return The ResponseEntity on security question info in JSON Format
	 */
	public ResponseObject getSecurityQuestionForUser(User user) {
		LOGGER.info("Start of getSecurityQuestionForUser()");

		String email = user.getEmail();
		String mobile = user.getMobile();

		ResponseObject response = new ResponseObject(HttpStatus.OK.value());

		Registration registration = registrationRepository.findByEmailAndActiveOrMobileAndActive(email, true, mobile,
				true);
		response.setData(registration.getSecurity());

		LOGGER.info("End of getSecurityQuestionForUser()");
		return response;
	}

	/**
	 * This method receives an input string and return the same. <br/>
	 * It is a dummy implementation of decryption. <br/>
	 * <br/>
	 * 
	 * @param msg
	 *            The message to decrypt.
	 * @return msg The decrypted message.
	 */
	private String decrypt(String msg) {
		return msg;
	}

	/**
	 * This method receives an input string and return MD5 encryption. <br/>
	 * <br/>
	 * 
	 * @param msg
	 *            The message to (MD5) encrypt
	 * @return MD5 encrypted message.
	 * @throws NoSuchAlgorithmException
	 */
	private String md5encrypt(String msg) throws NoSuchAlgorithmException {
		return encrypt(msg, "MD5");
	}

	/**
	 * This method receives an input string and return the encryption on the
	 * algorithm provided. <br/>
	 * <br/>
	 * 
	 * @param msg
	 *            The message to (MD5) encrypt
	 * @param algo
	 *            The encryption algorithm.
	 * @return The encrypted message.
	 * @throws NoSuchAlgorithmException
	 */
	private String encrypt(String msg, String algo) throws NoSuchAlgorithmException {
		MessageDigest md5 = MessageDigest.getInstance(algo);
		md5.update(msg.getBytes());
		byte msgBytes[] = md5.digest();

		StringBuffer msgBuff = new StringBuffer();
		for (int i = 0; i < msgBytes.length; i++) {
			msgBuff.append(Integer.toString((msgBytes[i] & 0xff) + 0x100, 16).substring(1));
		}

		return msgBuff.toString();
	}

	/**
	 * The sendOTP method sends the OTP for the user.<br/>
	 * <br/>
	 * 
	 * @param token
	 *            The token to validate.
	 * @param payload
	 *            The JSON payload received.
	 * @return The ResponseObject with the generated OTP.
	 */
	public ResponseObject sendOTP(User user) {
		String urlParameters = "authkey=%s&message=%s&sender=%s&mobile=%s&otp=%d&otp_expiry=&email=";
		ResponseObject response = null;
		
    String email = user.getEmail();
    String mobile = user.getMobile();
    String action = "registration";
    String message = "unsuccessful!";

    if (registrationRepository.findByEmailAndMobileAndActive(email, mobile, true) == null) {
      message = email + ": User not registered with us!";
      response = new ResponseObject(HttpStatus.CONFLICT.value(), message);
      response.setMessage(message);
      saveAuditTrail(email, action, "0.0.0.0", message);
      return response;
    }
		
		try {
			response = post(sendotpUrl, String.format(urlParameters, AUTHKEY, String.valueOf(user.getOtp()), SENDERID,
					user.getMobile(), user.getOtp()));
		} catch (QuantegaException e) {
			LOGGER.error(e.getMessage(), e);
		}

		return response;
	}

  /**
   * The resendOTP method resends the OTP for the user.<br/>
   * <br/>
   * 
   * @param token
   *            The token to validate.
   * @param payload
   *            The JSON payload received.
   * @return The ResponseObject with the generated OTP.
   */
  public ResponseObject resendOTP(User user) {
    String urlParameters = "authkey=%s&message=%s&sender=%s&mobile=%s&otp=%d&otp_expiry=&email=";
    ResponseObject response = null;
    
    /*
    if (registrationRepository.findByEmailAndMobileAndActive(email, mobile, true) == null) {
      message = email + ": User not registered with us!";
      response = new ResponseObject(HttpStatus.CONFLICT.value(), message);
      response.setMessage(message);
      saveAuditTrail(email, action, "0.0.0.0", message);
      return response;
    }
    */
    
    try {
      response = post(sendotpUrl, String.format(urlParameters, AUTHKEY, String.valueOf(user.getOtp()), SENDERID,
          user.getMobile(), user.getOtp()));
    } catch (QuantegaException e) {
      LOGGER.error(e.getMessage(), e);
    }

    return response;
  }

	/**
	 * The matchOTP method validates the OTP for the user.<br/>
	 * <br/>
	 * 
	 * @param token
	 *            The token to validate.
	 * @param payload
	 *            The JSON payload received.
	 * @return The ResponseObject with the matching OTP information.
	 */
	public ResponseObject matchOTP(User user) {
		String urlParameters = "authkey=%s&mobile=%s&otp=%d";
		ResponseObject response = null;
		try {
			response = post(matchotpUrl, String.format(urlParameters, AUTHKEY, user.getMobile(), user.getOtp()));
		} catch (QuantegaException e) {
			LOGGER.error(e.getMessage(), e);
		}
		return response;
	}

}
