package com.quantega.quantcare.services.service;

import java.util.Date;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.quantega.quantcare.services.dto.AuditTrail;
import com.quantega.quantcare.services.dto.DoctorProfile;
import com.quantega.quantcare.services.dto.Registration;
import com.quantega.quantcare.services.repositories.AuditTrailRepository;
import com.quantega.quantcare.services.repositories.DoctorProfileRepository;
import com.quantega.quantcare.services.repositories.RegistrationRepository;
import com.quantega.quantcare.services.security.bean.User;
import com.quantega.quantcare.services.security.service.UserAuthenticationService;
import com.quantega.quantcare.services.service.dto.ResponseObject;

/**
 * This class receives all the delegated services from DoctorProfileController.
 * So all the requests related to Profile are delegated here from the controller.<br/>
 * This includes doctor creation, edit a doctor and even deletion. <br/>
 * 
 * @author kalyan
 * @version 1.0
 */
@Service("doctorProfileService")
public final class DoctorProfileService implements IRestService {

  /** LOGGER is issued for logging purposes */
  private static final Logger LOGGER = LoggerFactory.getLogger(DoctorProfileService.class);

  @Autowired
  private DoctorProfileRepository doctorProfileRepository;
  
  @Autowired
  private AuditTrailRepository auditTrailRepository;

	@Autowired
	private UserAuthenticationService authenticationService;
	
	@Autowired
	private RegistrationRepository registrationRepository;
  
  /**
   * Profile service constructor.
   */
  public DoctorProfileService() {}

  /**
   * The register method is used for registering a new doctor with all the valid doctor information.<br/>
   * The doctor's registration related information is passed in the User object.<br/>
   * This registration doesn't require any token to authenticate. <br/>
   * <br/>
   * 
   * @param doctor   The doctor to create.
   * @return  The created doctor.
   */
  public ResponseObject create(DoctorProfile doctor) {
    
    LOGGER.info("Start of doctor profile-create()");

    String email = doctor.getEmail();
    String phoneNumber = doctor.getPhoneNumber();
    String action = "doctor profile-create";
    String message = "unsuccessful!";
    
    ResponseObject response = new ResponseObject(HttpStatus.CONFLICT.value(), message);
    
    if(doctorProfileRepository.findByEmailOrPhoneNumber(email, phoneNumber) != null) {
      message = email + ": Doctor already exists!";
      response.setMessage(message);
      saveAuditTrail(email, action, "0.0.0.0", message);
      return response;
    }

//    doctor.setCreatedBy("admin");
//    doctor.setCreatedOn(new Date());
//    doctor.setLastModifiedBy("admin");
//    doctor.setLastModifiedOn(new Date());
    doctor.setActive(true);
    
    doctorProfileRepository.save(doctor);
    message = "successful!";
    
    response.setStatus(HttpStatus.OK.value());
    response.setMessage(message);
    response.setData(doctor);
    
    saveAuditTrail(email, action, "0.0.0.0", message);
    LOGGER.info("End of doctor profile-create()");
    
    return response;
  }
  
  /**
   * The edit method edits an existing doctor.<br/>
   * The doctor content is received in REST call and saved back to the Profile through the Profile provided API calls<br/>
   * <br/>
 * @param token 
   * 
   * @param doctor   The doctor to edit.
   * @return  The edited doctor.
   */
  @CrossOrigin
  @ResponseBody
  @RequestMapping(value = "/edit", method = RequestMethod.POST)
	public ResponseObject edit(String token, DoctorProfile doctor) {

		LOGGER.info("Start of doctor profile-update()");

		Optional<User> sessionUser = authenticationService.findByToken(token);

		Registration registrationDetails = registrationRepository.findByEmailOrMobile(sessionUser.get().getUsername(),
				sessionUser.get().getUsername());

		String email = doctor.getEmail();
		String phoneNumber = doctor.getPhoneNumber();
		String action = "doctor profile-update";
		String message = "unsuccessful!";

		ResponseObject response = new ResponseObject(HttpStatus.CONFLICT.value(), message);

		DoctorProfile doctorProfile = doctorProfileRepository.findByEmailAndActiveOrPhoneNumberAndActive(
				registrationDetails.getEmail(), true, registrationDetails.getMobile(), true);

		/*
		 * if(doctorProfile == null) { message = email +
		 * ": Profile doesn't exists or registered with us!";
		 * response.setMessage(message); saveAuditTrail(email, action, "0.0.0.0",
		 * message); return response; }
		 */

		doctor.setId(doctorProfile.getId());

		doctorProfileRepository.save(doctor);
		if (!registrationDetails.getEmail().equals(doctor.getEmail())
				|| !registrationDetails.getMobile().equals(doctor.getPhoneNumber())) {

			registrationDetails.setEmail(doctor.getEmail());
			registrationDetails.setMobile(doctor.getPhoneNumber());
			registrationDetails.setCountryCode(doctor.getCountryCode());
			
			registrationRepository.save(registrationDetails);

			sessionUser.get().setUsername(doctor.getEmail());
		}

		message = "successful!";

		response.setStatus(HttpStatus.OK.value());
		response.setMessage(message);

		saveAuditTrail(email, action, "0.0.0.0", message);
		LOGGER.info("End of doctor profile-update()");

		return response;
	}

  /**
   * The publish method publishes the doctor just created.<br/>
   * The doctor publish is received in REST call and publishes the Profile through the Profile provided API calls<br/>
   * <br/>
   * 
   * @param doctor   The doctor to publish.
   * @return  The published doctor.
   */
  public ResponseObject get(DoctorProfile doctor) {
    
    LOGGER.info("Start of doctor profile-get()");

    String email = doctor.getEmail();
    String phoneNumber = doctor.getPhoneNumber();
    String action = "doctor profile-get";
    String message = "unsuccessful!";
    
    ResponseObject response = new ResponseObject(HttpStatus.CONFLICT.value(), message);
    
    DoctorProfile doctorProfile = doctorProfileRepository.findByEmailAndActiveOrPhoneNumberAndActive(email, true, phoneNumber, true);
    
    if(doctorProfile == null) {
      message = email + ": Profile doesn't exists or registered with us!";
      response.setMessage(message);
      saveAuditTrail(email, action, "0.0.0.0", message);
      return response;
    }

    doctorProfileRepository.save(doctor);
    message = "successful!";
    
    response.setStatus(HttpStatus.OK.value());
    response.setMessage(message);
    response.setData(doctorProfile);
    
    saveAuditTrail(email, action, "0.0.0.0", message);
    LOGGER.info("End of doctor profile-get()");
    
    return response;
  }

  /**
   * The delete method delete an existing doctor.<br/>
   * The doctor content is received in REST call and deleted from the Profile through the Profile provided API calls<br/>
   * <br/>
   * 
   * @param doctor   The doctor to delete.
   * @return  The deleted doctor.
   */
  public ResponseObject delete(DoctorProfile doctor) {
    
    LOGGER.info("Start of doctor profile-delete()");

    String email = doctor.getEmail();
    String phoneNumber = doctor.getPhoneNumber();
    String action = "doctor profile-delete";
    String message = "unsuccessful!";
    
    ResponseObject response = new ResponseObject(HttpStatus.CONFLICT.value(), message);
    
    DoctorProfile doctorProfile = doctorProfileRepository.findByEmailAndActiveOrPhoneNumberAndActive(email, true, phoneNumber, true);
    
    if(doctorProfile == null) {
      message = email + ": Profile doesn't exists or registered with us!";
      response.setMessage(message);
      saveAuditTrail(email, action, "0.0.0.0", message);
      return response;
    }
    
    doctorProfile.setActive(false);
    
    doctorProfileRepository.save(doctorProfile);
    message = "successful!";
    
    response.setStatus(HttpStatus.OK.value());
    response.setMessage(message);
    
    saveAuditTrail(email, action, "0.0.0.0", message);
    LOGGER.info("End of doctor profile-delete()");
    
    return response;
  }
  
  /**
   * This method handles the responsibility of saving in auduit_trail document. <br/>
   * All the relation information using path, action, access ip, message are sent in parameters. <br/> 
   * <br/>
   * 
   * @param path       The doctor's path of the action.
   * @param action      The action performed.
   * @param accessip    The IP of the request.
   * @param message     The message for the action.
   */
  private void saveAuditTrail(String path, String action, String accessip, String message) {
    saveAuditTrail(path, action, accessip, new Date(), message);
  }
  
  /**
   * This method handles the responsibility of savimg in auduit_trail document. <br/>
   * All the relation information using path, action, access time, access ip, message are sent in parameters. <br/> 
   * <br/>
   * 
   * @param path       The doctor's path of the action.
   * @param action      The action performed.
   * @param accesstime  The accesstime of the request.
   * @param accessip    The IP of the request.
   * @param message     The message for the action.
   */
  private void saveAuditTrail(String path, String action, String accessip, Date accesstime, String message) {
    AuditTrail auditTrail = new AuditTrail(path, action, accessip, accesstime, message);
    auditTrailRepository.save(auditTrail);
  }

}
