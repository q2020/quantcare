package com.quantega.quantcare.services.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.quantega.quantcare.services.dto.UserProfile;
import com.quantega.quantcare.services.repositories.UserProfileRepository;
import com.quantega.quantcare.services.service.dto.ResponseObject;

/**
 * This class receives all the delegated services from UserProfileController.
 * So all the requests related to Profile are delegated here from the controller.<br/>
 * This includes user creation, edit a user and even deletion. <br/>
 * 
 * @author kalyan
 * @version 1.0
 */
@Service("userProfileService")
public final class UserProfileService extends AbstractRestService implements IRestService {

  /** LOGGER is issued for logging purposes */
  private static final Logger LOGGER = LoggerFactory.getLogger(UserProfileService.class);

  @Autowired
  private UserProfileRepository userProfileRepository;
  
  /**
   * Profile service constructor.
   */
  public UserProfileService() {}

  /**
   * The register method is used for registering a new user with all the valid user information.<br/>
   * The user's registration related information is passed in the User object.<br/>
   * This registration doesn't require any token to authenticate. <br/>
   * <br/>
   * 
   * @param user   The user to create.
   * @return  The created user.
   */
  public ResponseObject create(UserProfile user) {
    
    LOGGER.info("Start of user profile-create()");

    String email = user.getEmail();
    String phoneNumber = user.getPhoneNumber();
    String action = "user profile-create";
    String message = "unsuccessful!";
    
    ResponseObject response = new ResponseObject(HttpStatus.CONFLICT.value(), message);
    
    if(userProfileRepository.findByEmailOrPhoneNumber(email, phoneNumber) != null) {
      message = email + ": User already exists!";
      response.setMessage(message);
      saveAuditTrail(email, action, "0.0.0.0", message);
      return response;
    }

    user.setActive(true);
    
    userProfileRepository.save(user);
    message = "successful!";
    
    response.setStatus(HttpStatus.OK.value());
    response.setMessage(message);
    response.setData(user);
    
    saveAuditTrail(email, action, "0.0.0.0", message);
    LOGGER.info("End of user profile-create()");
    
    return response;
  }
  
  /**
   * The edit method edits an existing user.<br/>
   * The user content is received in REST call and saved back to the Profile through the Profile provided API calls<br/>
   * <br/>
   * 
   * @param user   The user to edit.
   * @return  The edited user.
   */
  @CrossOrigin
  @ResponseBody
  @RequestMapping(value = "/edit", method = RequestMethod.POST)
  public ResponseObject edit(UserProfile user) {
    
    LOGGER.info("Start of user profile-update()");

    String email = user.getEmail();
    String phoneNumber = user.getPhoneNumber();
    String action = "user profile-update";
    String message = "unsuccessful!";
    
    ResponseObject response = new ResponseObject(HttpStatus.CONFLICT.value(), message);
    
    UserProfile userProfile = userProfileRepository.findByEmailAndActiveOrPhoneNumberAndActive(email, true, phoneNumber, true);
    
    if(userProfile == null) {
      message = email + ": User doesn't exists or registered with us!";
      response.setMessage(message);
      saveAuditTrail(email, action, "0.0.0.0", message);
      return response;
    }

    user.setId(userProfile.getId());
    user.setActive(userProfile.isActive());
    
    userProfileRepository.save(user);
    message = "successful!";
    
    response.setStatus(HttpStatus.OK.value());
    response.setMessage(message);
    
    saveAuditTrail(email, action, "0.0.0.0", message);
    LOGGER.info("End of user profile-update()");
    
    return response;
  }

  /**
   * The publish method publishes the user just created.<br/>
   * The user publish is received in REST call and publishes the Profile through the Profile provided API calls<br/>
   * <br/>
   * 
   * @param user   The user to publish.
   * @return  The published user.
   */
  public ResponseObject get(UserProfile user) {
    
    LOGGER.info("Start of user profile-get()");

    String email = user.getEmail();
    String phoneNumber = user.getPhoneNumber();
    String action = "user profile-get";
    String message = "unsuccessful!";
    
    ResponseObject response = new ResponseObject(HttpStatus.CONFLICT.value(), message);
    
    UserProfile userProfile = userProfileRepository.findByEmailAndActiveOrPhoneNumberAndActive(email, true, phoneNumber, true);
    
    if(userProfile == null) {
      message = email + ": User doesn't exists or registered with us!";
      response.setMessage(message);
      saveAuditTrail(email, action, "0.0.0.0", message);
      return response;
    }

    message = "successful!";
    
    response.setStatus(HttpStatus.OK.value());
    response.setMessage(message);
    response.setData(userProfile);
    
    saveAuditTrail(email, action, "0.0.0.0", message);
    LOGGER.info("End of user profile-get()");
    
    return response;
  }

  /**
   * The delete method delete an existing user.<br/>
   * The user content is received in REST call and deleted from the Profile through the Profile provided API calls<br/>
   * <br/>
   * 
   * @param user   The user to delete.
   * @return  The deleted user.
   */
  public ResponseObject delete(UserProfile user) {
    
    LOGGER.info("Start of user profile-delete()");

    String email = user.getEmail();
    String phoneNumber = user.getPhoneNumber();
    String action = "user profile-delete";
    String message = "unsuccessful!";
    
    ResponseObject response = new ResponseObject(HttpStatus.CONFLICT.value(), message);
    
    UserProfile userProfile = userProfileRepository.findByEmailAndActiveOrPhoneNumberAndActive(email, true, phoneNumber, true);
    
    if(userProfile == null) {
      message = email + ": User doesn't exists or registered with us!";
      response.setMessage(message);
      saveAuditTrail(email, action, "0.0.0.0", message);
      return response;
    }
    
    userProfile.setActive(false);
    
    userProfileRepository.save(userProfile);
    message = "successful!";
    
    response.setStatus(HttpStatus.OK.value());
    response.setMessage(message);
    
    saveAuditTrail(email, action, "0.0.0.0", message);
    LOGGER.info("End of user profile-delete()");
    
    return response;
  }
  
}
