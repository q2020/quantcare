package com.quantega.quantcare.services.service;

import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.quantega.quantcare.services.dto.AuditTrail;
import com.quantega.quantcare.services.repositories.AuditTrailRepository;
import com.quantega.quantcare.services.service.dto.ResponseObject;

/**
 * This class provides all the common functionalities for the REST services. <br/>
 * This common utility methods are shared and used by concrete individual services. <br/>
 * 
 * @author kalyan
 * @version 1.0
 */
public abstract class AbstractRestService implements IRestService {

  /** LOGGER is issued for logging purposes */
  private static final Logger LOGGER = LoggerFactory.getLogger(AbstractRestService.class);

  @Autowired
  protected AuditTrailRepository auditTrailRepository;
  
  /**
   * Authentication service constructor.
   */
  public AbstractRestService() {}

  /**
   * This method handles the responsibility of savimg in auduit_trail document. <br/>
   * All the relation information using email, action, access ip, message are sent in parameters. <br/> 
   * <br/>
   * 
   * @param email       The user's email of the action.
   * @param action      The action performed.
   * @param accessip    The IP of the request.
   * @param message     The message for the action.
   */
  protected void saveAuditTrail(String email, String action, String accessip, String message) {
    saveAuditTrail(email, action, accessip, new Date(), message);
  }
  
  /**
   * This method handles the responsibility of saving in auduit_trail document. <br/>
   * All the relation information using email, action, access ip, ResponseObject are sent in parameters. <br/> 
   * <br/>
   * 
   * @param email       The user's email of the action.
   * @param action      The action performed.
   * @param accessip    The IP of the request.
   * @param message     The message for the action.
   */
  protected void saveAuditTrail(String email, String action, String accessip, ResponseObject response) {
    saveAuditTrail(email, action, accessip, new Date(), response.toJson());
  }
  
  /**
   * This method handles the responsibility of savimg in auduit_trail document. <br/>
   * All the relation information using email, action, access time, access ip, message are sent in parameters. <br/> 
   * <br/>
   * 
   * @param email       The user's email of the action.
   * @param action      The action performed.
   * @param accesstime  The accesstime of the request.
   * @param accessip    The IP of the request.
   * @param message     The message for the action.
   */
  protected void saveAuditTrail(String email, String action, String accessip, Date accesstime, String message) {
    AuditTrail auditTrail = new AuditTrail(email, action, accessip, accesstime, message);
    LOGGER.info(String.format("Saving to audit-trail ... %s & %s", email, action));
    auditTrailRepository.save(auditTrail);
  }

}
