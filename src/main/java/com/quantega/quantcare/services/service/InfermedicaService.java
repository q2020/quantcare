package com.quantega.quantcare.services.service;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.quantega.quantcare.services.dto.InfermedicaGenericRequest;
import com.quantega.quantcare.services.dto.InfermedicaParseRequest;
import com.quantega.quantcare.services.exception.QuantegaException;
import com.quantega.quantcare.services.service.dto.ResponseObject;

/*
 * My Inc Confidential
 * Class: InfermedicaService.java
 * (C) Copyright My Inc. 2018
 * 
 * @author kk
 */
@Service("infermedicaService")
public class InfermedicaService extends AbstractRestService implements IRestService {

  /** LOGGER is issued for logging InfermedicaService incidences */
  private static final Logger LOGGER = LoggerFactory.getLogger(InfermedicaService.class);

  @Value("${infermedica.api.url}")
  private String apiURL;
  
  @Value("${infermedica.app.id}")
  private String appID;
  
  @Value("${infermedica.app.key}")
  private String appKEY;
  
  /** Map for headers */
  private Map<String, String> headers = new HashMap<>();
  
  /**
   * Infermedica service constructor.
   */
  public InfermedicaService() {
    
  }
  
  @PostConstruct
  private void init() {
    headers.put(APP_ID, appID);
    headers.put(APP_KEY, appKEY);
  }

  /**
   * The getConditions method returns a list of all available conditions to the caller.<br/>
   * 
   * @param id   The ID of the condition to retrieve.
   * @return  The ResponseObject with the condition/s.
   */
  public ResponseObject getConditions(String id) {
    String url = apiURL + "/conditions";
    if(id != null) {
      url += "/" + id;
    }
    
    ResponseObject response =  null;
    try {
      response = get(url, headers);
    } catch (QuantegaException e) {
      LOGGER.error(e.getMessage(), e);
    }
    return response;
  }
  
  /**
   * Suggests possible diagnoses and relevant observations based on provided patient information.<br/>
   * 
   * @param genericRequest  The generic request body object.
   * @return  The ResponseObject with the diagnoses and relevant observations based on provided patient information.
   */
  public ResponseObject diagnosis(InfermedicaGenericRequest genericRequest) {
    String url = apiURL + "/diagnosis";
    ResponseObject response =  null;
    try {
      response = post(url, toJson(genericRequest), headers);
    } catch (QuantegaException e) {
      LOGGER.error(e.getMessage(), e);
    }
    return response;
  }
  
  /**
   * Explains which evidence impact probability of selected condition.<br/>
   * 
   * @param genericRequest  The generic request body object.
   * @return response  The ResponseObject with the evidence impact probability of selected condition.
   */
  public ResponseObject explain(InfermedicaGenericRequest genericRequest) {
    String url = apiURL + "/explain";
    ResponseObject response =  null;
    try {
      response = post(url, toJson(genericRequest), headers);
    } catch (QuantegaException e) {
      LOGGER.error(e.getMessage(), e);
    }
    return response;
  }
  
  /**
   * Returns information about data used by diagnostic engine.<br/>
   * 
   * @return response The ResponseObject with the information about data used by diagnostic engine.
   */
  public ResponseObject info() {
    String url = apiURL + "/info";
    ResponseObject response =  null;
    try {
      response = get(url, headers);
    } catch (QuantegaException e) {
      LOGGER.error(e.getMessage(), e);
    }
    return response;
  }
  
  /**
   * Returns a single observation matching given phrase.<br/>
   * 
   * @return response The ResponseObject with a single observation matching given phrase.
   */
  public ResponseObject lookup() {
    String url = apiURL + "/lookup";
    ResponseObject response =  null;
    try {
      response = get(url, headers);
    } catch (QuantegaException e) {
      LOGGER.error(e.getMessage(), e);
    }
    return response;
  }
  
  /**
   * Returns list of mentions of observation found in given text.<br/>
   * 
   * @param parseRequest  The parse request body object.
   * @return response The ResponseObject with the list of mentions of observation found in given text.
   */
  public ResponseObject parse(InfermedicaParseRequest parseRequest) {
    String url = apiURL + "/parse";
    ResponseObject response =  null;
    try {
      response = post(url, toJson(parseRequest), headers);
    } catch (QuantegaException e) {
      LOGGER.error(e.getMessage(), e);
    }
    return response;
  }
  
  /**
   * The getRiskFactors method returns a list of all available risk factors to the caller.<br/>
   * 
   * @param id   The ID of the risk factor to retrieve.
   * @return  The ResponseObject with the risk factor/s.
   */
  public ResponseObject getRiskFactors(String id) {
    String url = apiURL + "/risk_factors";
    if(id != null) {
      url += "/" + id;
    }
    
    ResponseObject response =  null;
    try {
      response = get(url, headers);
    } catch (QuantegaException e) {
      LOGGER.error(e.getMessage(), e);
    }
    return response;
  }
  
  /**
   * The getLabTests method returns a list of all available lab tests to the caller.<br/>
   * 
   * @param id   The ID of the lab test to retrieve.
   * @return  The ResponseObject with the lab test/s.
   */
  public ResponseObject getLabTests(String id) {
    String url = apiURL + "/lab_tests";
    if(id != null) {
      url += "/" + id;
    }
    
    ResponseObject response =  null;
    try {
      response = get(url, headers);
    } catch (QuantegaException e) {
      LOGGER.error(e.getMessage(), e);
    }
    return response;
  }
  
  /**
   * The getSymptoms method returns a list of all available symptoms to the caller.<br/>
   * 
   * @param id   The ID of the symptom to retrieve.
   * @return  The ResponseObject with the symptom/s.
   */
  public ResponseObject getSymptoms(String id) {
    String url = apiURL + "/symptoms";
    if(id != null) {
      url += "/" + id;
    }
    
    ResponseObject response =  null;
    try {
      response = get(url, headers);
    } catch (QuantegaException e) {
      LOGGER.error(e.getMessage(), e);
    }
    return response;
  }
  
  /**
   * Returns list of observations matching the given phrase.<br/>
   * 
   * @param phrase  The phrase to search/
   * @param sex     The sex category
   * @param maxresults  Max results the search will return.
   * @return response The ResponseObject with the list of observations matching the given phrase.
   */
  public ResponseObject search(String phrase, String sex, String maxresults) {
    String url = apiURL + "/search?phrase=" + phrase + "&max_results=" + maxresults;
    
    if(sex != null) {
      url += "&sex=" + sex;
    }

    ResponseObject response =  null;
    try {
      response = get(url, headers);
    } catch (QuantegaException e) {
      LOGGER.error(e.getMessage(), e);
    }
    return response;
  }
  
  /**
   * Suggests possible symptoms based on provided patient information.<br/>
   * 
   * @param genericRequest  The generic request body object.
   * @return  The ResponseObject with the suggestions on possible symptoms based on provided patient information.
   */
  public ResponseObject suggest(InfermedicaGenericRequest genericRequest) {
    String url = apiURL + "/suggest";
    ResponseObject response =  null;
    try {
      response = post(url, toJson(genericRequest), headers);
    } catch (QuantegaException e) {
      LOGGER.error(e.getMessage(), e);
    }
    return response;
  }
  
  /**
   * Estimates triage level based on provided patient information.<br/>
   * 
   * @param genericRequest  The generic request body object.
   * @return response The ResponseObject with the estimation triage level based on provided patient information.
   */
  public ResponseObject triage(InfermedicaGenericRequest genericRequest) {
    String url = apiURL + "/triage";
    ResponseObject response =  null;
    try {
      response = post(url, toJson(genericRequest), headers);
    } catch (QuantegaException e) {
      LOGGER.error(e.getMessage(), e);
    }
    return response;
  }
  
}
