package com.quantega.quantcare.services.service;

import java.io.File;
import java.io.IOException;
import java.net.InetAddress;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.maxmind.geoip2.DatabaseReader;
import com.maxmind.geoip2.exception.GeoIp2Exception;
import com.maxmind.geoip2.model.CityResponse;
import com.maxmind.geoip2.record.City;
import com.maxmind.geoip2.record.Country;
import com.maxmind.geoip2.record.Location;
import com.maxmind.geoip2.record.Postal;
import com.maxmind.geoip2.record.Subdivision;
import com.quantega.quantcare.services.dto.IpGeoLocation;
import com.quantega.quantcare.services.service.dto.ResponseObject;

/**
 * This class receives all the delegated services from ConfigController. So all
 * the requests related to Profile are delegated here from the controller.<br/>
 * This includes retrieving GEO information from IP address provided. <br/>
 * 
 * @author kalyan
 * @version 1.0
 */
@Service("configService")
public final class ConfigService extends AbstractRestService implements IRestService {

	/** LOGGER is issued for logging purposes */
	private static final Logger LOGGER = LoggerFactory.getLogger(ConfigService.class);

  @Value("${mmdb.database}")
  private String mmdbDatabase;
  
  // This creates the DatabaseReader object. To improve performance, reuse
  // the object across lookups. The object is thread-safe.
  private DatabaseReader reader;
  
	/**
	 * Config service constructor.
	 */
	public ConfigService() {
	}

	@PostConstruct
  public void init() {
	  
    // A File object pointing to your GeoIP2 or GeoLite2 database
    File database = new File(mmdbDatabase);

    // Loading Geolite2 City database
    try {
      reader = new DatabaseReader.Builder(database).build();
    } catch (IOException e) {
      e.printStackTrace();
      LOGGER.error(e.getMessage(), e);
    }
	}

	public ResponseObject getGeoLocationByIP(String ip) {

		LOGGER.info("Start of getGeoLocationByIP(): " + ip);

		String action = "getGeoLocationByIP";

		ResponseObject response = new ResponseObject(HttpStatus.INTERNAL_SERVER_ERROR.value(), UNSUCCESSFUL);

		try {
      IpGeoLocation ipGeoLocation = findByIp(ip);
      response.setStatus(HttpStatus.OK.value());
      response.setData(ipGeoLocation);
      response.setMessage(SUCCESSFUL);
    } catch (IOException e) {
      e.printStackTrace();
      LOGGER.error(e.getMessage(), e);
      response.setError(e.getMessage());
    } catch (GeoIp2Exception e) {
      e.printStackTrace();
      LOGGER.error(e.getMessage(), e);
      response.setError(e.getMessage());
    }

		saveAuditTrail(ip, action, "0.0.0.0", response);
		LOGGER.info("End of getGeoLocationByIP()");

		return response;
	}

  /**
   * Find GEO location of the IP address passed in the argument.
   * 
   * @param  ipAddr The IP address to find GEO location.
   * @return IpGeoLocation containing GEO location of the IP address.
   * @throws IOException
   * @throws GeoIp2Exception
   */
  private IpGeoLocation findByIp(String ipAddr) throws IOException, GeoIp2Exception {
    InetAddress ipAddress = InetAddress.getByName(ipAddr);

    // Replace "city" with the appropriate method for your database, e.g.,
    // "country".
    CityResponse response = reader.city(ipAddress);
    if(response == null) {
      throw new GeoIp2Exception(ipAddress + ": not able to found any city location!");
    }
    
    Subdivision subdivision = response.getMostSpecificSubdivision();
    City city = response.getCity();
    Country country = response.getCountry();
    Postal postal = response.getPostal();

    Location location = response.getLocation();
    double latitude = location.getLatitude();
    double longitude = location.getLongitude();
    
    IpGeoLocation ipGeo = new IpGeoLocation(ipAddr, latitude, longitude,
        (city == null ? "" : city.getName()), 
        (subdivision == null ? "" : subdivision.getName()), (subdivision == null ? "" : subdivision.getIsoCode()),
        country.getName(), country.getIsoCode(),
        (postal == null ? "" : postal.getCode()));
    
    return ipGeo;
  }

}
