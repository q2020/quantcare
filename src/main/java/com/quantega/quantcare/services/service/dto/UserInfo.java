/**
 * 
 */
package com.quantega.quantcare.services.service.dto;

import java.util.Date;

/*
 * Quantega Confidential
 * Class: UserInfo.java
 * (C) Copyright Quantega Inc. 2018
 */

/**
 * @author kalyan
 *
 */
public class UserInfo {

  private String email;
  
  private String mobile;
  
  private String clientId;
  
  private String clientIp;
  
  private int clientPort;
  
  private Date lastaccessed;
  
  public UserInfo(String email, String mobile, String clientId, String clientIp, int clientPort, Date lastaccessed) {
    this.email = email;
    this.mobile = mobile;
    this.clientId = clientId;
    this.clientIp = clientIp;
    this.clientPort = clientPort;
    this.lastaccessed = lastaccessed;
  }

  /**
   * This method returns the email.
   * 
   * @return email the email
   */
  public String getEmail() {
    return email;
  }

  /**
   * This method sets the email.
   * 
   * @param email the email to set
   */
  public void setEmail(String email) {
    this.email = email;
  }

  /**
   * This method returns the mobile.
   * 
   * @return mobile the mobile
   */
  public String getMobile() {
    return mobile;
  }

  /**
   * This method sets the mobile.
   * 
   * @param mobile the mobile to set
   */
  public void setMobile(String mobile) {
    this.mobile = mobile;
  }

  /**
   * This method returns the clientId.
   * 
   * @return clientId the clientId
   */
  public String getClientId() {
    return clientId;
  }

  /**
   * This method sets the clientId.
   * 
   * @param clientId the clientId to set
   */
  public void setClientId(String clientId) {
    this.clientId = clientId;
  }

  /**
   * This method returns the clientIp.
   * 
   * @return clientIp the clientIp
   */
  public String getClientIp() {
    return clientIp;
  }

  /**
   * This method sets the clientIp.
   * 
   * @param clientIp the clientIp to set
   */
  public void setClientIp(String clientIp) {
    this.clientIp = clientIp;
  }

  /**
   * This method returns the clientPort.
   * 
   * @return clientPort the clientPort
   */
  public int getClientPort() {
    return clientPort;
  }

  /**
   * This method sets the clientPort.
   * 
   * @param clientPort the clientPort to set
   */
  public void setClientPort(int clientPort) {
    this.clientPort = clientPort;
  }

  /**
   * This method returns the lastaccessed.
   * 
   * @return lastaccessed the lastaccessed
   */
  public Date getLastaccessed() {
    return lastaccessed;
  }

  /**
   * This method sets the lastaccessed.
   * 
   * @param lastaccessed the lastaccessed to set
   */
  public void setLastaccessed(Date lastaccessed) {
    this.lastaccessed = lastaccessed;
  }

  /**
   * @return
   */
  public String toJson() {
    StringBuilder jsonObj = new StringBuilder();
    jsonObj.append("\"clientid\" :" + clientId);
    jsonObj.append("\"ipaddr\" : " + clientIp);
    jsonObj.append("\"port\" :" + clientPort);
    return jsonObj.toString();
  }
  
  
}
