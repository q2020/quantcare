/**
 * 
 */
package com.quantega.quantcare.services.service.dto;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

/*
 * Quantega Confidential
 * Class: ResponseObject.java
 * (C) Copyright Quantega Inc. 2018
 */

public class ResponseObject {

  /** The ObjectMapper instance */
  private static final ObjectMapper MAPPER = new ObjectMapper();
  
  /** The HTTP status */
  private int status;
  
  /** The HTTP message */
  private String message;
  
  /** The HTTP error if any */
  private String error;
  
  /** The HTTP data */
  private Object data;
  
  public ResponseObject() {
  }
  
  public ResponseObject(int httpStatus) {
    this.status = httpStatus;
  }

  public ResponseObject(int httpStatus, String message) {
    this.status = httpStatus;
    this.message = message;
  }
  /**
   * Getter for status
   * 
   * @return the status
   */
  public int getStatus() {
    return status;
  }

  
  /**
   * Setter for httpStatus to set.
   *
   * @param httpStatus the httpStatus to set
   */
  public void setStatus(int httpStatus) {
    this.status = httpStatus;
  }

  
  /**
   * Getter for message
   * 
   * @return the message
   */
  public String getMessage() {
    return message;
  }

  
  /**
   * Setter for message to set.
   *
   * @param message the message to set
   */
  public void setMessage(String message) {
    this.message = message;
  }

  
  /**
   * Getter for error
   * 
   * @return the error
   */
  public String getError() {
    return error;
  }

  
  /**
   * Setter for error to set.
   *
   * @param error the error to set
   */
  public void setError(String error) {
    this.error = error;
  }

  
  /**
   * Getter for data
   * 
   * @return the data
   */
  public Object getData() {
    return data;
  }

  
  /**
   * Setter for data to set.
   *
   * @param data the data to set
   */
  public void setData(Object data) {
    this.data = data;
  }

  public String toJson() {
    StringBuilder jsonObj = new StringBuilder();
    jsonObj.append("{ ");
    jsonObj.append("\"status\" :" + status);
    if(message != null) jsonObj.append(",\"message\": " + "\"" + message + "\"");
    if(error != null) jsonObj.append(",\"error\" : " + "\"" + error + "\"");
    
    if(data != null) {
      try {
        jsonObj.append(",\"data\": " + MAPPER.writeValueAsString(data));
      } catch (JsonProcessingException e) {
        e.printStackTrace();
      }
    }
    
    jsonObj.append(" }");
    
    return jsonObj.toString();

  }
}
