package com.quantega.quantcare.services.service;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import javax.net.ssl.HttpsURLConnection;

import org.apache.commons.codec.binary.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.quantega.quantcare.services.dto.Registration;
import com.quantega.quantcare.services.exception.QuantegaException;
import com.quantega.quantcare.services.service.dto.ResponseObject;

/*
 *  Confidential
 * Class: IRestService.java
 * (C) Copyright Quantega Inc. 2018
 */
public interface IRestService {

  /** LOGGER is issued for logging purposes */
  static final Logger LOGGER = LoggerFactory.getLogger(IRestService.class);


  /** Tokens are saved here */
  Map<String, Registration> TOKENUSERMAP = new ConcurrentHashMap<>();

  /** Tokens are saved here */
  Map<String, String> USERTOKENMAP = new ConcurrentHashMap<>();

  /** DateFormat Instance */
  SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

  /** Calendar Instance */
  Calendar calendar = new GregorianCalendar();

  /** ObjectMapper instance */
  ObjectMapper MAPPER = new ObjectMapper();
  
  /** successful */
  String SUCCESSFUL = "successful";

  /** unsuccessful */
  String UNSUCCESSFUL = "unsuccessful";

  /** _id */
  String ID = "_id";

  /** User */
  String USER = "user";

  /** Users */
  String USERS = "users";

  /** Doctor */
  String DOCTOR = "doctor";

  /** Doctors */
  String DOCTORS = "doctors";

  /** Sessions */
  String SESSIONS = "sessions";

  /** Username */
  String USERNAME = "username";

  /** PasswordObject */
  String PASSWORD = "password";

  /** Name */
  String NAME = "name";

  /** Index */
  String INDEX = "index";
  
  /** Type */
  String TYPE = "type";
  
  /** App Id */
  String APP_ID = "App-Id";
  
  /** App Key */
  String APP_KEY = "App-Key";
  
  /** Magnolia URI to publish */
  String MAGNOLIA_PUBLISH_URL = "/activate";
  
  /** Magnolia URI to unpublish */
  String MAGNOLIA_UNPUBLISH_URL = "/deactivate";
  
  default ResponseEntity<String> validate() {
    ResponseObject responseObj = new ResponseObject();
    responseObj.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());
    responseObj.setError("License has expired");
    return new ResponseEntity<String>(responseObj.toJson(), HttpStatus.INTERNAL_SERVER_ERROR);
  }
  
  /**
   * HTTP Post call is made with the post URL and its parameters passed in arguments.
   * 
   * @param urlStr  The URL String
   * @param contentType The Content-Type provided
   * @return response   The call result in ResponseObject
   * @throws QuantegaException 
   */
  default ResponseObject get(String urlStr) throws QuantegaException {
    return get(urlStr, MediaType.APPLICATION_JSON_VALUE);
  }
  
  /**
   * HTTP Post call is made with the post URL and its parameters passed in arguments.
   * 
   * @param urlStr  The URL String
   * @param contentType The Content-Type provided
   * @return response   The call result in ResponseObject
   * @throws QuantegaException 
   */
  default ResponseObject get(String urlStr, String contentType) throws QuantegaException {
    return get(urlStr, null, contentType);
  }
  
  /**
   * HTTP Post call is made with the post URL and its parameters passed in arguments.
   * 
   * @param urlStr  The URL String
   * @param contentType The Content-Type provided
   * @return response   The call result in ResponseObject
   * @throws QuantegaException 
   */
  default ResponseObject get(String urlStr, Map<String, String> headers) throws QuantegaException {
    return get(urlStr, headers, MediaType.APPLICATION_JSON_VALUE);
  }
  
  /**
   * HTTP Post call is made with the post URL and its parameters passed in arguments.
   * 
   * @param urlStr  The URL String
   * @param headers The headers map
   * @param contentType The Content-Type provided
   * @return response   The call result in ResponseObject
   * @throws QuantegaException 
   */
  default ResponseObject get(String urlStr, Map<String, String> headers, String contentType) throws QuantegaException {
    return get(urlStr, false, null, null, headers, contentType);
  }
  
  /**
   * HTTP/HTTPS Post call is made with the post URL and its parameters passed in arguments.
   * 
   * @param urlStr  The URL String
   * @param urlParameters The URL parameters
   * @param isHttps if it is a https request.
   * @param headers The headers map
   * @param contentType The Content-Type provided
   * @return response   The call result in ResponseObject
   * @throws QuantegaException 
   */
  default ResponseObject get(String urlStr, boolean isHttps, String username, String password, Map<String, String> headers, String contentType) throws QuantegaException {
  
    ResponseObject response = new ResponseObject(HttpStatus.BAD_REQUEST.value(), "Failure!");
    
    HttpURLConnection conn = null;
    
    try {
      URL url = new URL(urlStr);
      conn = isHttps ? (HttpsURLConnection) url.openConnection() : (HttpURLConnection) url.openConnection();
  
      //Add reuqest header
      conn.setRequestMethod("GET");
      conn.setRequestProperty("Accept-Language", "en-US,en;q=0.5");
      
      // Adding headers
      if(headers != null) {
        for(String key : headers.keySet()) {
          conn.addRequestProperty(key, headers.get(key));
        }
      }
      
      if(username != null && password != null) {
        String usernamepassword = username + ":" + password;
        String basicAuth = "Basic " + new String(new Base64().encode(usernamepassword.getBytes()));
        conn.setRequestProperty("Authorization", basicAuth);
      }

      int responseCode = conn.getResponseCode();
      response.setStatus(responseCode);
      
      System.out.println("\nSending 'GET' request to URL : " + url);
      System.out.println("Response Code : " + responseCode);
  
      // Data stream retrieval
      StringBuffer sb = new StringBuffer();
      InputStream stream = (responseCode == HttpStatus.OK.value()) ? conn.getInputStream() : conn.getErrorStream();
      try (BufferedReader in  = new BufferedReader(new InputStreamReader(stream))) {
      
        String inputLine;
        while ((inputLine = in.readLine()) != null) {
          sb.append(inputLine);
        }
      }
      catch(Exception e) {
        LOGGER.error(e.getMessage(), e);
      }
      
      response.setMessage((responseCode == HttpStatus.OK.value()) ? SUCCESSFUL : UNSUCCESSFUL);
      if(responseCode == HttpStatus.OK.value()) response.setData(sb.toString()); else response.setError(sb.toString());
        
    } catch (MalformedURLException e) {
      throw new QuantegaException(e.getMessage(), e);
    } catch (IOException e) {
      throw new QuantegaException(e.getMessage(), e);
    } catch (Exception e) {
      throw new QuantegaException(e.getMessage(), e);
    }
    finally {
      try {
        if(conn != null) {
          conn.disconnect();;
        }
      }
      catch(Exception e) {
        // Nothing to do
      }
    }
    
    return response;
  }
  
  /**
   * HTTP Post call is made with the post URL and its parameters passed in arguments.
   * 
   * @param urlStr  The URL String
   * @param urlParameters The URL parameters
   * @return response   The call result in ResponseObject
   * @throws QuantegaException 
   */
  default ResponseObject post(String urlStr, String urlParameters) throws QuantegaException {
    return post(urlStr, urlParameters, null, MediaType.APPLICATION_JSON_VALUE);
  }
  
  /**
   * HTTP Post call is made with the post URL and its parameters passed in arguments.
   * 
   * @param urlStr  The URL String
   * @param urlParameters The URL parameters
   * @param contentType The Content-Type provided
   * @return response   The call result in ResponseObject
   * @throws QuantegaException 
   */
  default ResponseObject post(String urlStr, String urlParameters, String contentType) throws QuantegaException {
    return post(urlStr, urlParameters, null, contentType);
  }
  
  /**
   * HTTP Post call is made with the post URL and its parameters passed in arguments.
   * 
   * @param urlStr  The URL String
   * @param urlParameters The URL parameters
   * @param contentType The Content-Type provided
   * @return response   The call result in ResponseObject
   * @throws QuantegaException 
   */
  default ResponseObject post(String urlStr, String urlParameters, Map<String, String> headers) throws QuantegaException {
    return post(urlStr, urlParameters, headers, MediaType.APPLICATION_JSON_VALUE);
  }
  
  /**
   * HTTP Post call is made with the post URL and its parameters passed in arguments.
   * 
   * @param urlStr  The URL String
   * @param urlParameters The URL parameters
   * @param headers The headers map
   * @param contentType The Content-Type provided
   * @return response   The call result in ResponseObject
   * @throws QuantegaException 
   */
  default ResponseObject post(String urlStr, String urlParameters, Map<String, String> headers, String contentType) throws QuantegaException {
    return post(urlStr, urlParameters, false, null, null, headers, contentType);
  }
  
  /**
   * HTTP/HTTPS Post call is made with the post URL and its parameters passed in arguments.
   * 
   * @param urlStr  The URL String
   * @param urlParameters The URL parameters
   * @param isHttps if it is a Https: request.
   * @param headers The headers map
   * @param contentType The Content-Type provided
   * @return response   The call result in ResponseObject
   * @throws QuantegaException 
   */
  default ResponseObject post(String urlStr, String urlParameters, boolean isHttps, String username, String password, Map<String, String> headers, String contentType) throws QuantegaException {
  
    ResponseObject response = new ResponseObject(HttpStatus.BAD_REQUEST.value(), "Failure!");
    
    DataOutputStream dos = null;
    HttpURLConnection conn = null;
    
    try {
      URL url = new URL(urlStr);
      conn = isHttps ? (HttpsURLConnection) url.openConnection() : (HttpURLConnection) url.openConnection();
  
      //Add reuqest header
      conn.setRequestMethod("POST");
      conn.setRequestProperty("Accept-Language", "en-US,en;q=0.5");
      if(contentType != null) {
        conn.addRequestProperty("Content-Type", contentType);
      }
      
      // Adding headers
      if(headers != null) {
        for(String key : headers.keySet()) {
          conn.addRequestProperty(key, headers.get(key));
        }
      }
      
      if(username != null && password != null) {
        String usernamepassword = username + ":" + password;
        String basicAuth = "Basic " + new String(new Base64().encode(usernamepassword.getBytes()));
        conn.setRequestProperty("Authorization", basicAuth);
      }

      // Send post request
      conn.setDoOutput(true);
      dos = new DataOutputStream(conn.getOutputStream());
      dos.writeBytes(urlParameters);
      dos.flush();
  
      int responseCode = conn.getResponseCode();
      response.setStatus(responseCode);
      
      System.out.println("\nSending 'POST' request to URL : " + url);
      System.out.println("Post parameters : " + urlParameters);
      System.out.println("Response Code : " + responseCode);
  
      // Data stream retrieval
      StringBuffer sb = new StringBuffer();
      InputStream stream = (responseCode == HttpStatus.OK.value()) ? conn.getInputStream() : conn.getErrorStream();
      try (BufferedReader in  = new BufferedReader(new InputStreamReader(stream))) {
      
        String inputLine;
        while ((inputLine = in.readLine()) != null) {
          sb.append(inputLine);
        }
      }
      catch(Exception e) {
        LOGGER.error(e.getMessage(), e);
      }
      
      response.setMessage((responseCode == HttpStatus.OK.value()) ? SUCCESSFUL : UNSUCCESSFUL);
      if(responseCode == HttpStatus.OK.value()) response.setData(sb.toString()); else response.setError(sb.toString());
  
    } catch (MalformedURLException e) {
      throw new QuantegaException(e.getMessage(), e);
    } catch (IOException e) {
      throw new QuantegaException(e.getMessage(), e);
    } catch (Exception e) {
      throw new QuantegaException(e.getMessage(), e);
    }
    finally {
      try {
        if(dos != null) {
          dos.close();
        }
        if(conn != null) {
          conn.disconnect();;
        }
      }
      catch(Exception e) {
        // Nothing to do
      }
    }
    
    return response;
  }
  
  default String toJson (Object obj) {
    String json = null;
    try {
      json = MAPPER.writeValueAsString(obj);
    } catch (JsonProcessingException e) {
      e.printStackTrace();
    }
    
    return json;
  }
  
  default Object toObject (String content, Class<?> clazz) {
    Object obj = null;
    try {
      obj = MAPPER.readValue(content, clazz);
    } catch (JsonProcessingException e) {
      e.printStackTrace();
    } catch (IOException e) {
      e.printStackTrace();
    }
    
    return obj;
  }
}
