package com.quantega.quantcare.services.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.quantega.quantcare.services.dto.Aadhaar;
import com.quantega.quantcare.services.exception.QuantegaException;
import com.quantega.quantcare.services.service.dto.ResponseObject;

/**
 * This class receives all the delegated services from AadhaarService.
 * So all the requests related to aadhaar are delegated here from the controller.<br/>
 * 
 * @author kalyan
 * @version 1.0
 */
@Service("aadhaarService")
public final class AadhaarService implements IRestService {

  /** LOGGER is issued for logging purposes */
  private static final Logger LOGGER = LoggerFactory.getLogger(AadhaarService.class);

  @Value("${aadhaar.version}")
  private String AADHAAR_VERSION;
  
  @Value("${aadhaar.asalk}")
  private String AADHAAR_LICENSE_KEY;
  
  @Value("${aadhaar.url}")
  private String aadhaarUrl;
  
  /**
   * Authentication service constructor.
   */
  public AadhaarService() {}

  /**
   * The matchAadhaar method validates the OTP for the user.<br/>
   * <br/>
   * 
   * @param token   The token to validate.
   * @param payload The JSON payload received.
   * @return  The ResponseObject with the matching OTP information.
   */
  public ResponseObject getAadhaar(Aadhaar aadhaar) {
    String urlParameters = "authkey=%s&mobile=%s&otp=%d";
    ResponseObject response =  null;
    try {
      response = post(aadhaarUrl, String.format(urlParameters, AADHAAR_LICENSE_KEY));
    } catch (QuantegaException e) {
      LOGGER.error(e.getMessage(), e);
    }
    return response;
  }
  
  /**
   * The matchAadhaar method validates the OTP for the user.<br/>
   * <br/>
   * 
   * @param token   The token to validate.
   * @param payload The JSON payload received.
   * @return  The ResponseObject with the matching OTP information.
   */
  public ResponseObject matchAadhaar(Aadhaar aadhaar) {
    String urlParameters = "authkey=%s&mobile=%s&otp=%d";
    ResponseObject response =  null;
    try {
      response = post(aadhaarUrl, String.format(urlParameters, AADHAAR_LICENSE_KEY));
    } catch (QuantegaException e) {
      LOGGER.error(e.getMessage(), e);
    }
    return response;
  }
  
}
