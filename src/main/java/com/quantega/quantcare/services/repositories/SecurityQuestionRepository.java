package com.quantega.quantcare.services.repositories;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.quantega.quantcare.services.dto.SecurityQuestion;

/*
 * My Inc Confidential
 * Class: SecurityQuestionRepository.java
 * (C) Copyright My Inc. 2018
 * 
 * @author kk
 */
public interface SecurityQuestionRepository extends MongoRepository<SecurityQuestion, String> {

  public List<SecurityQuestion> findByActive(boolean active);

}
