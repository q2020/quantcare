package com.quantega.quantcare.services.repositories;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.quantega.quantcare.services.dto.DoctorProfile;

/*
 * My Inc Confidential
 * Class: DoctorProfileRepository.java
 * (C) Copyright My Inc. 2018
 * 
 * @author kk
 */
public interface DoctorProfileRepository extends MongoRepository<DoctorProfile, String> {

  public DoctorProfile findByEmailOrPhoneNumber(String email, String phoneNumber);
  public DoctorProfile findByEmailAndActiveOrPhoneNumberAndActive(String email, boolean eActive, String phoneNumber, boolean pActive);

}
