package com.quantega.quantcare.services.repositories;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.quantega.quantcare.services.dto.CMSPage;

/*
 * My Inc Confidential
 * Class: CMSPageRepository.java
 * (C) Copyright My Inc. 2018
 * 
 * @author kk
 */
public interface CMSRepository extends MongoRepository<CMSPage, String> {

  public CMSPage findByPathAndRepositoryAndRecursive(String path, String repository, boolean recursive);
  public CMSPage findByPathAndRecursive(String path, boolean recursive);
  public CMSPage findByPath(String path);

}
