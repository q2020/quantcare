package com.quantega.quantcare.services.repositories;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.quantega.quantcare.services.dto.User;

/*
 * My Inc Confidential
 * Class: UserRepository.java
 * (C) Copyright My Inc. 2018
 * 
 * @author kk
 */
public interface UserRepository extends MongoRepository<User, String> {

  public User findByEmailAndPassword(String email, String password);
  public User findByEmail(String email);

}
