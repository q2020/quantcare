package com.quantega.quantcare.services.repositories;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.quantega.quantcare.services.dto.Registration;

/*
 * My Inc Confidential
 * Class: RegistrationRepository.java
 * (C) Copyright My Inc. 2018
 * 
 * @author kk
 */
public interface RegistrationRepository extends MongoRepository<Registration, String> {

  public Registration findByEmailAndSecurityAndAnswerAndActiveOrMobileAndSecurityAndAnswerAndActive(String email, String security, String answer, Boolean active, String mobile, String msecurity, String manswer, Boolean mactive);
  public Registration findByEmailAndPasswordAndActiveOrMobileAndPasswordAndActive(String email, String password, boolean active, String mobile, String mpassword, boolean mactive);
  public Registration findByEmailAndActiveOrMobileAndActive(String email, boolean active, String mobile, boolean mactive);
  public Registration findByEmailAndMobileAndActive(String email, String mobile, boolean mactive);
  public Registration findByEmailOrMobile(String email, String mobile);

}
