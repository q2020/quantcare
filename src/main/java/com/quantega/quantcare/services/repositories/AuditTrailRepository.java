package com.quantega.quantcare.services.repositories;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.quantega.quantcare.services.dto.AuditTrail;

/*
 * My Inc Confidential
 * Class: AuditTrailRepository.java
 * (C) Copyright My Inc. 2018
 * 
 * @author kk
 */
public interface AuditTrailRepository extends MongoRepository<AuditTrail, String> {

}
