package com.quantega.quantcare.services.repositories;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.quantega.quantcare.services.dto.Doctor;

/*
 * My Inc Confidential
 * Class: DoctorRepository.java
 * (C) Copyright My Inc. 2018
 * 
 * @author kk
 */
public interface DoctorRepository extends MongoRepository<Doctor, String> {

  public Doctor findByFirstName(String firstName);
  public List<Doctor> findByLastName(String lastName);

}
