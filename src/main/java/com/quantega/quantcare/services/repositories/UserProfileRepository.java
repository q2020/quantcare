package com.quantega.quantcare.services.repositories;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.quantega.quantcare.services.dto.UserProfile;

/*
 * My Inc Confidential
 * Class: UserProfileRepository.java
 * (C) Copyright My Inc. 2018
 * 
 * @author kk
 */
public interface UserProfileRepository extends MongoRepository<UserProfile, String> {

  public UserProfile findByEmailOrPhoneNumber(String email, String phoneNumber);
  public UserProfile findByEmailAndActiveOrPhoneNumberAndActive(String email, boolean eActive, String phoneNumber, boolean pActive);

}
